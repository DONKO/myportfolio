import React from 'react';
import './QuantityWrapper.scss';

type QuantityWrapperProps = {
    quantity: number;
    itemId: string;
    updateQuantity: (id: string, newQuantity: number) => void;
}

function QuantityWrapper ({quantity,itemId,updateQuantity,}: QuantityWrapperProps){

    const increaseQuantity = () => {
        updateQuantity(itemId, quantity + 1);
    };

    const decreaseQuantity = () => {
        if (quantity > 1) {
            updateQuantity(itemId, quantity - 1);
        }
    };

    const handleQuantityChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newQuantity = e.target.value === '' ? 0 : Number(e.target.value);
        if (!isNaN(newQuantity) && newQuantity >= 0) {
            updateQuantity(itemId, newQuantity);
        }
    };

    return (
        <div className='quantity-wrapper'>
            <div className="value-button decrease" onClick={decreaseQuantity}>-</div>
            <input
                type="text"
                className="number"
                value={quantity === 0 ? 0 : quantity}
                onChange={handleQuantityChange}
            />
            <div className="value-button increase" onClick={increaseQuantity}>+</div>
        </div>
    );
};

export default QuantityWrapper;
