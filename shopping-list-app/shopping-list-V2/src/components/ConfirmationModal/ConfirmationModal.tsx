import React from 'react';
import './ConfirmationModal.scss';

type ConfirmationModalProps = {
    isOpen: boolean;
    message: string;
    onConfirm: () => void;
    onCancel: () => void;
};

function ConfirmationModal({isOpen, message, onConfirm, onCancel}:ConfirmationModalProps){
    if (!isOpen) return null;

    return (
        <div className="modal-backdrop">
            <div className="modal">
                <p className='mb-10 mt-10'>{message}</p>
                <button onClick={onConfirm} className='btn danger'>Confirm</button>
                <button onClick={onCancel} className='btn secondary'>Cancel</button>
            </div>
        </div>
    );
};

export default ConfirmationModal;
