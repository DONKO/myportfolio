import { routes } from "../routes";


function NavbarDesktop() {
    return (
        <ul className="flex items-center gap-5 text-sm mr-5">
            {routes.map((route) => {
                const { href, title, key, Icon } = route;
                return (
                    <li key={key} >
                        <a
                            href={href}
                            className="flex items-center gap-1 hover:text-neutral-400 transition-all"
                        >
                            <Icon />
                            {title}
                        </a>
                    </li>
                );
            })}
        </ul>
    );
};

export default NavbarDesktop;