import React, { Dispatch, ReactNode, SetStateAction, createContext, useContext, useEffect, useState } from 'react';
import productsData from '../data/productsData/products.json'
import { Product } from '../types/Types';

type ProductContextType = {
    products: Product[];
    setProducts: Dispatch<SetStateAction<Product[]>>;
};

const ProductContext = createContext<ProductContextType | undefined>(undefined);

export const useProducts = (): ProductContextType => {
    const context = useContext(ProductContext);
    if (!context) throw new Error('useProducts must be used within a ProductProvider');
    return context;
};

export const ProductProvider = ({ children }: { children: ReactNode }) => {
    const [products, setProducts] = useState<Product[]>([]);

    useEffect(() => {
        setProducts(productsData.products);
    }, []);

    return (
        <ProductContext.Provider value={{ products, setProducts }}>
            {children}
        </ProductContext.Provider>
    );
};
