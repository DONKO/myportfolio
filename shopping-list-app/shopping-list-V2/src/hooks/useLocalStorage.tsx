import { useState, useEffect } from 'react';
import { ListItem } from '../types/Types';


function useShoppingList(storage_key: string) {
    const [shoppingList, setShoppingList] = useState<ListItem[]>(() => {
        const savedList = localStorage.getItem(storage_key);
        if (savedList) {
            const parsedList = JSON.parse(savedList);
            return parsedList.map((item: ListItem) => ({ ...item, checked: false }));
        } else {
            return [];
        }
    });

    // Save the shopping list to localStorage whenever it changes
    useEffect(() => {
        localStorage.setItem(storage_key, JSON.stringify(shoppingList));
    }, [shoppingList, storage_key]);

    const addItem = (item: ListItem) => {
        setShoppingList((currentList) => [...currentList, item]);
    };

    const removeItem = (itemId: string) => {
        setShoppingList((currentList) => currentList.filter(item => item.id !== itemId));
    };

    const updateItem = (itemId: string, newItem: ListItem) => {
        setShoppingList((currentList) => {
            const itemIndex = currentList.findIndex(item => item.id === itemId);
            if (itemIndex > -1) {
                const updatedList = [...currentList];
                updatedList[itemIndex] = newItem;
                return updatedList;
            }
            return currentList;
        });
    };

    const updateQuantity = (itemId: string, newQuantity: number) => {
        setShoppingList((currentList) => {
            const updatedList = currentList.map(item => {
                if (item.id === itemId) {
                    return { ...item, quantity: newQuantity };
                }
                return item;
            });
            return updatedList;
        });
    }


    return { shoppingList, setShoppingList, addItem, removeItem, updateItem, updateQuantity };
}

export default useShoppingList;