import React, { useState, useEffect, useRef } from 'react';
import DeleteIcon from '@mui/icons-material/Delete';
import Toast from '../components/Toast/Toast';
import ConfirmationModal from '../components/ConfirmationModal/ConfirmationModal';
import './ShoppingList.scss'
import { ListItem, Product } from '../types/Types';
import useShoppingList from '../hooks/useLocalStorage';
import InputSuggestions from '../components/InputSuggestions/InputSuggestions';
import ShoppingListItems from '../components/ShoppingListItem/ShoppingListItems';
import { useProducts } from '../contexts/ProductContext';


function ShoppingList() {
    const { products } = useProducts();
    const [inputValue, setInputValue] = useState('');
    const [suggestions, setSuggestions] = useState<Product[]>([]);
    const [isInputEmpty, setIsInputEmpty] = useState(true);
    const listSearchRef = useRef<HTMLDivElement>(null);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [isToastOpen, setIsToastOpen] = useState(false);
    const [toastMessage, setToastMessage] = useState('');
    const [toastType, setToastType] = useState<"success" | "error">("success");
    const { shoppingList, setShoppingList, addItem, updateItem, updateQuantity } = useShoppingList("shoppingList");

    useEffect(() => {
        if (inputValue === '') {
            setSuggestions([]);
        } else {
            setSuggestions(products.filter(product =>
                product.title.toLowerCase().includes(inputValue.toLowerCase())
            ));
        }
    }, [inputValue, products]);

    useEffect(() => {   // Used for graying the background when searching items
        function handleClickOutside(event: MouseEvent) {
            if (listSearchRef.current && !listSearchRef.current.contains(event.target as Node)) {
                setIsInputEmpty(true);
            }
        }
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [listSearchRef]);

    const handleAddItem = ({ title, id }: { title: string; id: string }) => {
        const product = products.find(p => p.title.toLowerCase() === title.toLowerCase());
        if (product) {
            const existingItemIndex = shoppingList.findIndex(p => p.title.toLowerCase() === title.toLowerCase());
            if (existingItemIndex > -1) {
                const newItems = [...shoppingList];
                updateQuantity(newItems[existingItemIndex].id, newItems[existingItemIndex].quantity + 1);
            } else {
                addItem({ ...product, checked: false, quantity: 1 });
            }
        } else {
            const newItem: ListItem = { title, id, checked: false, quantity: 1 };
            setShoppingList(currentList => [...currentList, newItem]);
        }
        setToastMessage('Item Added Successfully!');
        setToastType("success");
        setIsToastOpen(true);
        setInputValue('');
    };

    const deleteChecked = (): void => {
        // const checkedItems = shoppingList.filter(item => item.checked);
        // checkedItems.forEach(item => removeItem(item.id));

        // Optimized way for removing multiple items:
        const newShoppingList = shoppingList.filter(item => !item.checked);
        setShoppingList(newShoppingList);

        setToastMessage('Items Successfully Removed!');
        setToastType("success");
        setIsToastOpen(true);
    };

    const handleConfirm = () => {
        deleteChecked();
        setIsModalOpen(false);
    };

    const handleCancel = () => {
        setIsModalOpen(false);
    };

    const handleModalOpen = () => {
        const checkedItems = shoppingList.filter(item => item.checked);
        if (checkedItems.length > 0) {
            setIsModalOpen(true);
        }
    }


    return (
        <div className={`shopping-list-wrapper app-root ${isInputEmpty ? '' : 'gray-out'}`}>
            <div ref={listSearchRef} className="container mx-auto list-search">
                <div className="flex items-center justify-between bg-gray-800 p-4 rounded-t-lg">
                    <h1 className="text-white text-lg mb-0">Shopping List</h1>
                    <button onClick={() => handleModalOpen()} className="...">
                        <DeleteIcon style={{ color: 'white' }} />
                    </button>
                </div>

                <InputSuggestions
                    inputValue={inputValue}
                    suggestions={suggestions}
                    isInputEmpty={isInputEmpty}
                    setInputValue={setInputValue}
                    handleAddItem={handleAddItem}
                    setIsInputEmpty={setIsInputEmpty}
                />
            </div>

            <ShoppingListItems
                shoppingList={shoppingList}
                updateItem={updateItem}
                updateQuantity={updateQuantity}
            />

            <Toast
                isOpen={isToastOpen}
                message={toastMessage}
                type={toastType}
                onClose={() => {
                    setIsToastOpen(false);
                }}
            />

            <ConfirmationModal
                isOpen={isModalOpen}
                message="Are you sure you want Remove the selected items from your Shopping List?"
                onConfirm={handleConfirm}
                onCancel={handleCancel}
            />
        </div>
    );
}

export default ShoppingList;