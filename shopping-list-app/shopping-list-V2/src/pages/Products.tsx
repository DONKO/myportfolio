import React, { useState } from 'react';
import fallbackImage from '../data/images/fallback.png';
import './Products.scss'
import { useNavigate } from 'react-router-dom';
import Toast from '../components/Toast/Toast';
import { Product, ListItem } from '../types/Types';
import useShoppingList from '../hooks/useLocalStorage';
import InfoButton from '../components/InfoButton/InfoButton';
import { useProducts } from '../contexts/ProductContext';


function Products() {
    const { products } = useProducts();
    const [isToastOpen, setIsToastOpen] = useState(false);
    const { shoppingList, addItem } = useShoppingList("shoppingList");
    let navigate = useNavigate();

    const addToCart = (product: Product) => {
        const productInCart = shoppingList.find((item: Product) => item.id === product.id);
        if (!productInCart) {
            const addToShoppingListItem: ListItem = {
                id: product.id,
                title: product.title,
                base64Image: product.base64Image || fallbackImage,
                price: product.price,
                description: product.description,
                checked: false,
                quantity: 1
            };
            addItem(addToShoppingListItem)
            setIsToastOpen(true);
        }
    };

    const navigateToProduct = (productId: string,) => {
        navigate(`/product/${productId}`);
    }


    return (
        <div className="products-container">
            <div className='page-title-info'>
                <h1>Our Products</h1>
                <InfoButton htmlContent="
                    <p>Let your heart explore the wonderful products our shelves offer for your Shopping List...</p>
                    <p>Click on a product to view speciffic information abot it or simply Add it to your Shopping List by clicking Add to List.</p>
                    <p>Happy Shopping &#128512;</p>
                " />
            </div>
            {products.map((product, index) => {
                const productInCart = shoppingList.find((item: Product) => item.id === product.id);
                return (
                    <div key={product.id} className="product-item">
                        <div onClick={() => navigateToProduct(product.id)} className='h-full'>
                        {/* <div onClick={() => navigateToProduct(product, !!productInCart)} className='h-full'> */}
                            <div className='h-[90%] max-h-[310px] flex flex-col justify-center mb-[2%]'>
                                <img src={product.base64Image ? product.base64Image : fallbackImage} alt={product.title} />
                            </div>
                            <div>{product.title}</div>
                        </div>
                        {productInCart ? (
                            <button onClick={() => window.location.href = '/shoppingList'} className='btn secondary'>Go to List</button>
                        ) : (
                            <button onClick={() => addToCart(product)} className='btn primary'>Add to List</button>
                        )}
                    </div>
                );
            })}

            <Toast
                isOpen={isToastOpen}
                message='Item added successfully!'
                type='success'
                onClose={() => setIsToastOpen(false)}
            />
        </div>
    );
}

export default Products;