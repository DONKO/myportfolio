import ListIcon from '@mui/icons-material/List';
import StoreIcon from '@mui/icons-material/Store';

export const routes = [
    {
        title: "Store",
        href: "/",
        key: 1,
        Icon: StoreIcon,
    },
    {
        title: "ShoppingList",
        href: "/shoppingList",
        key: 2,
        Icon: ListIcon,
    }
];