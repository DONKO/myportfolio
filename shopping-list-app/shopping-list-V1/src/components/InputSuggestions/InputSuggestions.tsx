import React from 'react';
import { Product } from '../../types/Types';
import fallbackImage from '../../data/images/fallback.png';
import './InputSuggestions.scss'

interface InputSuggestionsProps {
    inputValue: string;
    isInputEmpty: boolean;
    setInputValue: React.Dispatch<React.SetStateAction<string>>;
    setIsInputEmpty: React.Dispatch<React.SetStateAction<boolean>>;
    suggestions: Product[];
    handleAddItem: (itemTitle: string) => void;
}

function InputSuggestions({ inputValue, isInputEmpty, setInputValue, setIsInputEmpty, suggestions, handleAddItem }: InputSuggestionsProps) {
    return (
        <div className="flex flex-col sm:flex-row items-center gap-4 p-4 suggestion-input-wrapper">
            <input
                type="text"
                value={inputValue}
                placeholder="+ Add item"
                onChange={(e) => {
                    setInputValue(e.target.value);
                    setIsInputEmpty(e.target.value === '');
                }}
                className="w-full border-2 px-1 suggestion-input"
            />
            {/* <button onClick={() => addItem(inputValue)} className="w-28">
                        Add to list
                </button> */}
            {!isInputEmpty &&
                <div className="suggestions ">
                    {suggestions.map((suggestion, index) => (
                        <div key={index}
                            onClick={() => {
                                handleAddItem(suggestion.title);
                                setIsInputEmpty(true);
                            }}
                            className="list-item-wrapper px-4 py-4 flex items-center sm:flex-row">
                            <div className="list-item-description">
                                <img src={suggestion.base64Image ? suggestion.base64Image : fallbackImage} alt={suggestion.title} className="h-12 w-12 rounded-full ..." />
                                <span className="ml-3">{suggestion.title}</span>
                            </div>
                        </div>
                    ))}
                </div>
            }
        </div>
    );
}

export default InputSuggestions;
