import React, { useEffect } from 'react';
import './Toast.scss';

type ToastProps = {
	isOpen: boolean;
	message: string;
	type: 'success' | 'error';
	onClose: () => void;
};

function Toast({ isOpen, message, type, onClose }: ToastProps) {
	useEffect(() => {
		let timer: NodeJS.Timeout;
		if (isOpen) {
			timer = setTimeout(() => {
				onClose();
			}, 3000);
		}
		return () => clearTimeout(timer);
	}, [isOpen, onClose]);

	if (!isOpen) return null;

	const typeStyles = {
		success: 'bg-green-500 text-white',
		error: 'bg-red-500 text-white',
	};

	return (
		<div className={`toast ${typeStyles[type]}`}>
			<span>{message}</span>
		</div>
	);
};

export default Toast;