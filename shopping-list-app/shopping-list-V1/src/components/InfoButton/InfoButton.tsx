import React, { useState } from 'react';
import './InfoButton.scss';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';

type InfoButtonProps = {
    htmlContent: string;
}

function InfoButton({htmlContent}:InfoButtonProps){
    const [isHovered, setIsHovered] = useState(false);

    return (
        <div className="info-button-container">
            <div
                className="info-icon"
                onMouseEnter={() => setIsHovered(true)}
                onMouseLeave={() => setIsHovered(false)}
            >
                <InfoOutlinedIcon/>
            </div>
            {isHovered && (
                <div
                    className="info-content"
                    dangerouslySetInnerHTML={{ __html: htmlContent }}
                />
            )}
        </div>
    );
};

export default InfoButton;
