import React from 'react';
import { ListItem, Product } from '../../types/Types';
import fallbackImage from '../../data/images/fallback.png';
import { useNavigate } from 'react-router-dom';
import QuantityWrapper from '../QuantityWrapper/QuantityWrapper';
import './ShoppingListItems.scss';


type ShoppingListItemProps = {
    shoppingList: ListItem[];
    updateItem: (id: string, updatedItem: ListItem) => void;
    updateQuantity: (id: string, quantity: number) => void;
};

function ShoppingListItems({ shoppingList, updateItem, updateQuantity }: ShoppingListItemProps) {
    let navigate = useNavigate();

    const toggleCheck = (index: number): void => {
        const newItems = [...shoppingList];
        newItems[index].checked = !newItems[index].checked;
        updateItem(newItems[index].id, newItems[index]); //example using the updateItem function, 
                                                            //perhaps a dedicated function similar to updateQuantity would be more appropriate,
                                                            //but non the less this is an example of updating items entire content
    };

    const navigateToProduct = (product: Product, productInCart: Boolean) => {
        navigate(`/product`, { state: { product: product, productInCart: productInCart } });
    }


    return (
        <div className="container mx-auto bg-white shadow overflow-hidden sm:rounded-md listed-items">
            {shoppingList.length > 0 ?
                <>
                    {shoppingList.map((item, index) => (
                        <div key={index} className="list-item-wrapper px-4 py-4 flex items-center sm:flex-row">
                            <div className="list-item-description" onClick={() => navigateToProduct(item, true)}>
                                <img src={item.base64Image ? item.base64Image : fallbackImage} alt={item.title} className="h-12 w-12 rounded-full ..." />
                                <span className="ml-3">{item.title}</span>
                            </div>
                            <QuantityWrapper
                                quantity={item.quantity}
                                itemId={item.id}
                                updateQuantity={(id, quantity) => updateQuantity(id, quantity)}
                            />
                            <div className="list-item-check">
                                <input
                                    type="checkbox"
                                    checked={item.checked}
                                    onChange={() => toggleCheck(index)}
                                    className="..."
                                />
                            </div>
                        </div>
                    ))}
                </>
                :
                <div className="px-4 py-4 sm:flex-row text-center">
                    <h2>Your list is Empty!</h2>
                    <p className='text-gray-300'>Add some products above.</p>
                </div>
            }
        </div>
    );
}
export default ShoppingListItems;
