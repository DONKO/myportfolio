import React from 'react';
import './App.scss';
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import ShoppingList from './pages/ShoppingList';
import Products from './pages/Products';
import ProductDetailsPage from './pages/ProductDetailsPage';
import NavbarMain from './components/Navbar/NavbarMain';

function App() {
	return (
		<div className="App">
			<Router>
				<NavbarMain />
				<Routes>
					<Route path="/shoppingList" element={<ShoppingList />}></Route>
					<Route path="/" element={<Products />}></Route>
					<Route path="/product" element={<ProductDetailsPage />} />
				</Routes>
			</Router>
		</div>
	);
}

export default App;
