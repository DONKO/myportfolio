export type Product = {
    id: string;
    title: string;
    base64Image: string;
    price?: number;
    description?: string;
};

export type ListItem = Product & {
    checked: boolean;
    quantity: number;
};