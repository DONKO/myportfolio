import NavbarMobile from "./NavbarMobile";
import NavbarDesktop from "./NavbarDesktop";
import logoImage from '../../data/images/logo.svg';
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";
import './NavbarMain.scss';


function NavbarMain() {
    const [isScrolled, setIsScrolled] = useState(false);
    const [isMobile, setIsMobile] = useState(window.innerWidth <= 1024);

    useEffect(() => {   // to handle which navbar to show
        const handleResize = () => {
            setIsMobile(window.innerWidth <= 1024);
        };

        window.addEventListener('resize', handleResize);
        handleResize();

        return () => window.removeEventListener('resize', handleResize);
    }, []);
    
    useEffect(() => {   // to add shadow under the navbar, when user scrolls the page
        const handleScroll = () => {
            const isUserScrolled = window.scrollY > 50;
            setIsScrolled(isUserScrolled);
        };
        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    }, []);


    return (
        <div className={`fixed top-0 left-0 right-0 bg-neutral-950 border-b border-neutral-700 text-white h-12 nav-wrapper wave ${isScrolled ? 'shadow' : ''}`}>
            <nav className="container flex items-center justify-between lg:py-2 max-w-full">
                <Link to="/" className="max-w-10 pl-2">
                    <img src={logoImage} alt="Logo of the website" />
                </Link>
                {isMobile ? <NavbarMobile /> : <NavbarDesktop />}
            </nav>
        </div>
    );
};

export default NavbarMain;