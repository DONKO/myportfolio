import React from 'react';
import { ListItem } from '../../types/Types';
import fallbackImage from '../../data/images/fallback.png';
import { useNavigate } from 'react-router-dom';
import QuantityWrapper from '../QuantityWrapper/QuantityWrapper';
import './ShoppingListItems.scss';
import { useShoppingListDispatch, useShoppingListState } from '../../contexts/ShoppingListContextReducer';


function ShoppingListItems() {
    let navigate = useNavigate();
    const dispatch = useShoppingListDispatch();
    const { lists, activeListId } = useShoppingListState();
    const activeShoppingList = lists.find(list => list.id === activeListId);
    const activeShoppingListItems = activeShoppingList?.items || [];

    const toggleCheck = (itemId: string): void => {
        const index = activeShoppingListItems.findIndex(item => item.id === itemId);
        //change the checked property of the item to the opposite of what it currently is and get the new ListItem
        if (index !== -1) {
            const newItem: ListItem = { ...activeShoppingListItems[index], checked: !activeShoppingListItems[index].checked };
            dispatch({ type: 'UPDATE_ITEM', itemId: itemId, newItem: newItem });
        }
    }

    const navigateToProduct = (productId: string,) => {
        navigate(`/product/${productId}`);
    }

    const totalPrice = parseFloat(activeShoppingListItems.reduce((total, item) => total + (item.price || 0) * item.quantity, 0).toFixed(2));

    const updateQuantity = (itemId: string, newQuantity: number) => {
        dispatch({ type: 'UPDATE_QUANTITY', itemId, newQuantity });
    };

    return (
        <div className="container mx-auto bg-white shadow overflow-hidden sm:rounded-md listed-items">
            {activeShoppingListItems.length > 0 ?
                <>
                    {activeShoppingListItems.map((item, index) => (
                        <div key={index} className="list-item-wrapper px-4 py-4">
                            <div className="list-item-description" onClick={() => navigateToProduct(item.id)}>
                                <img src={item.base64Image ? item.base64Image : fallbackImage} alt={item.title} className="h-12 w-12 rounded-full ..." />
                                <span className="ml-3">{item.title}</span>
                            </div>
                            <QuantityWrapper
                                quantity={item.quantity}
                                itemId={item.id}
                                updateQuantity={(itemId, newQuantity) => updateQuantity(itemId, newQuantity)}
                            />
                            <div className="list-item-price flex w-[35%]">
                            {item.price &&
                                <>
                                    <div className="ml-3">Price: {item.price}</div>
                                    {item.quantity &&
                                        <div className="ml-3">Item Total: {item.price && item.price * item.quantity}</div>
                                    }
                                </>
                            }
                            </div>
                            <div className="list-item-check">
                                <input
                                    type="checkbox"
                                    checked={item.checked}
                                    onChange={() => toggleCheck(item.id)}
                                    className="..."
                                />
                            </div>
                        </div>
                    ))}
                    <p className='mt-2 mb-4 text-center '>Max Total: {totalPrice}</p>
                </>
                :
                <div className="px-4 py-4 sm:flex-row text-center">
                    <h2>Your list is Empty!</h2>
                    <p className='text-gray-300'>Add some products above.</p>
                </div>
            }
        </div>
    );
}
export default ShoppingListItems;
