import React, { useState } from 'react';
import { useShoppingListDispatch, useShoppingListState } from '../../contexts/ShoppingListContextReducer';
import Toast from '../Toast/Toast';
import './ShoppingListsMain.scss';


function ShoppingListsMain() {
    const { lists, activeListId } = useShoppingListState();
    const [isToastOpen, setIsToastOpen] = useState(false);
    const [toastMessage, setToastMessage] = useState('');
    const [toastType, setToastType] = useState<"success" | "error">("success");
    const dispatch = useShoppingListDispatch();
    const [isOptionsModalOpen, setIsOptionsModalOpen] = useState(false);
    const [listToDelete, setListToDelete] = useState('')

    const [newListName, setNewListName] = useState('');
    const handleAddList = (): void => {
        if (newListName.trim() !== '') {
            dispatch({
                type: 'ADD_LIST',
                name: newListName,
            });
            setNewListName('');
            setToastMessage('List Added Successfully!');
            setToastType("success");
            setIsToastOpen(true);
        }
    };

    const handleSetActiveList = (listId: string): void => {
        dispatch({
            type: 'SET_ACTIVE_LIST',
            listId: listId,
        });

        toggleOptionsModal()
    };

    const handleRemoveList = () => {
        if (listToDelete && lists.length > 1) {
            dispatch({
                type: 'REMOVE_LIST',
                listId: listToDelete,
            });
            setToastMessage('List Removed Successfully!');
            setToastType("error");
            setIsToastOpen(true);
        }
        // TO-DO: add modal...
    };

    const toggleOptionsModal = () => {
        setIsOptionsModalOpen(!isOptionsModalOpen);
    };

    return (
        <div className='shopping-list-main-wrapper'>

            <div className='shopping-lists-active-name' onClick={toggleOptionsModal}>{lists.find((list) => list.id === activeListId)?.name}</div>

            {isOptionsModalOpen && (
                <div className="modal-backdrop">
                    <div className="modal">
                        <div className='shopping-lists-options'>
                            <div className='text-right' onClick={toggleOptionsModal}>x</div>
                            <div>Select the Active List:</div>
                            <select className='shopping-lists-options-select' onChange={(e) => handleSetActiveList(e.target.value)} value={activeListId || ""}>
                                {lists.map((list) => (
                                    <option key={list.id} value={list.id}>
                                        {list.name}
                                    </option>
                                ))}
                            </select>

                            <hr className='mt-4 mb-4' />

                            <div>Select the list you wish to delete:</div>
                            <select className='shopping-lists-options-select shopping-lists-options-delete' onChange={(e) => setListToDelete(e.target.value)}>
                                <option></option>
                                {lists.map((list) => (
                                    <option key={list.id} value={list.id}>
                                        {list.name}
                                    </option>
                                ))}
                            </select>
                            <button className='btn danger w-full' onClick={handleRemoveList}>Remove List</button>

                            <hr className='mt-4 mb-4' />
                            <div className="add-list-form">
                                <div>Add a new List:</div>
                                <input
                                    className='add-list-form-input'
                                    type="text"
                                    value={newListName}
                                    onChange={(e) => setNewListName(e.target.value)}
                                    placeholder="Enter new list name"
                                />
                                <button className='btn secondary w-full' onClick={handleAddList}>Add List</button>
                            </div>

                            <Toast
                                isOpen={isToastOpen}
                                message={toastMessage}
                                type={toastType}
                                onClose={() => {
                                    setIsToastOpen(false);
                                }}
                            />
                        </div>
                    </div>
                </div>
            )}

        </div>
    );
};

export default ShoppingListsMain;
