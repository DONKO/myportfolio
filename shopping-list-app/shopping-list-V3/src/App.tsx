import React from 'react';
import './App.scss';
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import ShoppingList from './pages/ShoppingList';
import Products from './pages/Products';
import ProductDetailsPage from './pages/ProductDetailsPage';
import NavbarMain from './components/Navbar/NavbarMain';
import { ProductProvider } from './contexts/ProductContext';
import { ShoppingListProvider } from './contexts/ShoppingListContextReducer';

function App() {
	return (
		<div className="App">
			<ProductProvider>
				<ShoppingListProvider>
					<Router>
						<NavbarMain />
						<Routes>
							<Route path="/shoppingList" element={<ShoppingList />}></Route>
							<Route path="/" element={<Products />}></Route>
							{/* <Route path="/product" element={<ProductDetailsPage />} /> */}
							<Route path="/product/:productId" element={<ProductDetailsPage />} />
						</Routes>
					</Router>
				</ShoppingListProvider>
			</ProductProvider>
		</div>
	);
}

export default App;
