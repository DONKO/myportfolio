import React, { createContext, useReducer, useContext, useEffect } from 'react';
import { ListItem } from '../types/Types';

type ShoppingList = {
    id: string;
    name: string;
    items: ListItem[];
};

type State = {
    lists: ShoppingList[];
    activeListId: string | null;
};

type Action =
    | { type: 'ADD_ITEM'; item: ListItem }
    | { type: 'REMOVE_ITEM'; itemId: string }
    | { type: 'UPDATE_ITEM'; itemId: string; newItem: ListItem }
    | { type: 'UPDATE_QUANTITY'; itemId: string; newQuantity: number }
    | { type: 'SET_ITEMS'; items: ListItem[] }
    | { type: 'ADD_LIST'; name: string }
    | { type: 'REMOVE_LIST'; listId: string }
    | { type: 'SET_ACTIVE_LIST'; listId: string };

const initialState: State = {
    lists: [{
        id: 'default_list_id',
        name: 'My Shopping List',
        items: [],
    }],
    activeListId: 'default_list_id', // Set the default list as the active list
};

const ShoppingListStateContext = createContext<State | undefined>(undefined);
const ShoppingListDispatchContext = createContext<React.Dispatch<Action> | undefined>(undefined);

function shoppingListReducer(state: State, action: Action): State {
    switch (action.type) {
        case 'ADD_ITEM': {
            const newList = state.lists.map(list => {
                if (list.id === state.activeListId) {
                    return { ...list, items: [...list.items, action.item] };
                }
                return list;
            });
            return { ...state, lists: newList };
        }
        case 'REMOVE_ITEM': {
            const newList = state.lists.map(list => {
                if (list.id === state.activeListId) {
                    return { ...list, items: list.items.filter(item => item.id !== action.itemId) };
                }
                return list;
            });
            return { ...state, lists: newList };
        }
        case 'UPDATE_ITEM': {
            const newList = state.lists.map(list => {
                if (list.id === state.activeListId) {
                    return {
                        ...list,
                        items: list.items.map(item => item.id === action.itemId ? action.newItem : item),
                    };
                }
                return list;
            });
            return { ...state, lists: newList };
        }
        case 'UPDATE_QUANTITY': {
            const newList = state.lists.map(list => {
                if (list.id === state.activeListId) {
                    return {
                        ...list,
                        items: list.items.map(item =>
                            item.id === action.itemId ? { ...item, quantity: action.newQuantity } : item
                        ),
                    };
                }
                return list;
            });
            return { ...state, lists: newList };
        }
        case 'SET_ITEMS': {
            const newList = state.lists.map(list => {
                if (list.id === state.activeListId) {
                    return { ...list, items: action.items };
                }
                return list;
            });
            return { ...state, lists: newList };
        }
        case 'ADD_LIST': {
            const newListId = `list_${Date.now()}`;
            const newList = {
                id: newListId,
                name: action.name,
                items: [],
            };
            return {
                ...state,
                lists: [...state.lists, newList],
                activeListId: newListId, // setting the new list as active
            };
        }
        case 'REMOVE_LIST': {
            const listsAfterRemoval = state.lists.filter(list => list.id !== action.listId);
            return {
                ...state,
                lists: listsAfterRemoval,
                activeListId: listsAfterRemoval.length ? listsAfterRemoval[0].id : null,
            };
        }
        case 'SET_ACTIVE_LIST': {
            return {
                ...state,
                activeListId: action.listId,
            };
        }
        default:
            throw new Error(`Unhandled action type: ${action}`);
    }
}

const ShoppingListProvider: React.FC<{ children: React.ReactNode }> = ({ children }) => {
    const storage_key = 'shoppingList';
    const [state, dispatch] = useReducer(shoppingListReducer, initialState, () => {
        const savedLists = localStorage.getItem(storage_key);
        if (savedLists) {
            let parsedList = JSON.parse(savedLists);
            parsedList.lists = parsedList.lists.map((list: ShoppingList) => ({
                ...list,
                items: list.items.map((item: ListItem) => ({ ...item, checked: false }))
            }));
            return parsedList;
        }
        return initialState;
    });

    useEffect(() => {
        localStorage.setItem(storage_key, JSON.stringify(state));
    }, [state]);

    return (
        <ShoppingListStateContext.Provider value={state}>
            <ShoppingListDispatchContext.Provider value={dispatch}>
                {children}
            </ShoppingListDispatchContext.Provider>
        </ShoppingListStateContext.Provider>
    );
};

function useShoppingListState() {
    const context = useContext(ShoppingListStateContext);
    if (!context) {
        throw new Error('useShoppingListState must be used within a ShoppingListProvider');
    }
    return context;
}

function useShoppingListDispatch() {
    const context = useContext(ShoppingListDispatchContext);
    if (!context) {
        throw new Error('useShoppingListDispatch must be used within a ShoppingListProvider');
    }
    return context;
}

export { ShoppingListProvider, useShoppingListState, useShoppingListDispatch };