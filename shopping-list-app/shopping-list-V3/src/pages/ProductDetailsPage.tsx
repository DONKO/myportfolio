import { useEffect, useMemo, useState } from 'react';
import fallbackImage from '../data/images/fallback.png';
import Toast from '../components/Toast/Toast';
import { Product, ListItem } from '../types/Types';
import { useParams } from 'react-router-dom';
import { useProducts } from '../contexts/ProductContext';
import { useShoppingListDispatch, useShoppingListState } from '../contexts/ShoppingListContextReducer';


function ProductDetailsPage() {
    const { productId } = useParams<{ productId: string }>();
    const [product, setProduct] = useState<Product>()
    const [isProductInCart, setIsProductInCart] = useState<Boolean>()
    const [isToastOpen, setIsToastOpen] = useState(false);
    const { products } = useProducts();
    const dispatch = useShoppingListDispatch();

    const { lists, activeListId } = useShoppingListState();

    const activeShoppingListItems = useMemo(() => {
        const activeShoppingList = lists.find(list => list.id === activeListId);
        return activeShoppingList?.items || [];
    }, [lists, activeListId]);


    useEffect(() => {
        const product = products.find((item: Product) => item.id === productId);
        if (product) {
            setProduct(product)
        }
        const productInCart = activeShoppingListItems.find((item: Product) => item.id === productId);
        if (productInCart) {
            setIsProductInCart(!!productInCart)
        }
    }, [products, activeShoppingListItems, productId]);

    const addToCart = (product: Product) => {
        const productInCart = activeShoppingListItems.find((item: Product) => item.id === product.id);
        if (!productInCart) {
            const addToShoppingListItem: ListItem = {
                id: product.id,
                title: product.title,
                base64Image: product.base64Image || fallbackImage,
                price: product.price,
                description: product.description,
                checked: false,
                quantity: 1
            };
            dispatch({ type: 'ADD_ITEM', item: addToShoppingListItem });
            setIsProductInCart(true)
            setIsToastOpen(true);
        }
    };


    return (
        <div>
            <div className='shopping-list-wrapper app-root'>
                <div className="container mx-auto mt-5 mb-5 list-search">
                    <div className="flex items-center justify-between bg-gray-800 p-4 rounded-t-lg">
                        <h1 className="text-white text-lg">{product ? product.title : "Product Information"}</h1>
                    </div>
                    <div className="container mx-auto bg-white shadow overflow-hidden sm:rounded-md listed-items">
                        {product ?
                            <div className='flex flex-col p-5 justify-center'>
                                <img className='max-w-lg m-auto w-full' src={product.base64Image ? product.base64Image : fallbackImage} alt={product.title} />
                                <p>{product.description}</p>
                                <p className='my-3'><b>Price: {product.price} EUR</b></p>
                                {isProductInCart ? (
                                    <button onClick={() => window.location.href = '/shoppingList'} className='btn secondary'>Go to List</button>
                                ) : (
                                    <button onClick={() => addToCart(product)} className='btn primary'>Add to List</button>
                                )}
                            </div>
                            :
                            <div className='m-2'>
                                The product data could not be found! Please try again.
                            </div>
                        }
                    </div>
                </div>
            </div>

            <Toast
                isOpen={isToastOpen}
                message='Item Added Successfully!'
                type='success'
                onClose={() => setIsToastOpen(false)}
            />
        </div>
    );
}

export default ProductDetailsPage;