# shopping-list


## Run the app
```
npm install
npm start
```

## Versions, Pages and Functionalities
### V1: (cd shopping-list-V1)
#### Shopping List page:
    URL: /shoppingList
    functionalities:
    - View all products/items added to the Shopping List.
    - View, increase, decrease the quantity of a product on the Shopping List.
    - Check, uncheck items.
    - Delete checked items by clicking on the delete icon.
    - Click on the item image or title to go to the Product details page (/product).
    - Search for an item by writing its title in the input search bar:
        - Items that match the search value will appear as suggestions.
        - Add an item by clicking on it
            - If an item is already on the Shopping List the quantity for that item will increase.


#### Products aka Shop page:
    URL: /
    functionalities:
    - View all products/items available.
    - Add an item to the Shopping List by clicking on the button "Add to List"
    - or click on the button "Go to List" to go to the Shopping List page (/shoppingList) if the item is already on the Shopping List.
    - Click on the image or item title to go to the Product details page (/product).

#### Product Details page
    URL: /product 
    functionalities:
    - view extended product information
    - Add an item to the Shopping List by clicking on the button "Add to List"
    - or click on the button "Go to List" to go to the Shopping List page (/shoppingList) if the item is already on the Shopping List.

### V2: (cd shopping-list-V2)
    The same functionalities as mentioned above in V1.
    +++++
    - Product Context (useContext()) (replaces the loading of all static products in required pages).
    - Added Price and Cost Calculation on the Shopping list page.
    - The Product details Page has a custom URL with the item ID, so it could potentially be shared.
    - Adding custom Shopping List Items (items that are not in the Products), by writing the item name in the search box and clicking on Add Custom Item.

### V3: (cd shopping-list-V3)
    The same functionalities as mentioned above in V2.
    +++++
    - Shopping List Context (useContext()) (replaces the loading of ShoppingList in required pages).
    - Shopping List Reducer (useReducer()) (to handle Shopping List state across the entire app).
    - Added the functionality of having multiple lists (Add list, Remove list and Select the active list) to the Shopping List Page.
    - ...


