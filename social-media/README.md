# social-media


## Run the app
```
- Backend:
    npm install
    "nodemon server.js" or "npm start"
- Frontend:
    npm install
    npm run start
```

## Versions, Pages and Functionalities
### V1: (cd social-media-V1)
- sign Up
- log In
- view, edit profile
- view other people profiles
- follow/unfollow users
- search users
- view all posts
- view posts from people you follow
- post: 
    - edit (self)
    - delete (self)
    - like/unlike
    - comment
    - delete your comment (self)

- translations (implemented funcionality, but not translated)
    - switch translation

- light/dark theme
- create new post
- confirmation popups
- toasts for when something is executed or error

