const mongoose = require('mongoose')
const { ObjectId } = mongoose.Schema.Types

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    followers: [{ 
        type: ObjectId, ref: "User" 
    }],
    following: [{ 
        type: ObjectId, ref: "User" 
    }],
    profilePicture: {
        type: String,
        default: "https://res.cloudinary.com/dr1iyzezy/image/upload/v1708624635/pvw8nfu2lmcm7zmbupp0.png"
    }
})


const User = mongoose.model('User', userSchema);
module.exports = User;