const jwt = require('jsonwebtoken');
const { JWT_SECRET } = require('../config/keys');
const User = require('../models/user');
const middleware = require('./requireLogin');

jest.mock('jsonwebtoken');
jest.mock('../models/user');

describe('Authentication Middleware', () => {
  let mockRequest;
  let mockResponse;
  let nextFunction;

  beforeEach(() => {
    mockRequest = {
      headers: {},
    };
    mockResponse = {
      status: jest.fn(() => mockResponse),
      json: jest.fn(),
    };
    nextFunction = jest.fn();
    User.findById = jest.fn();
  });

  it('should return an error if no authorization token is found', () => {
    middleware(mockRequest, mockResponse, nextFunction);
    expect(mockResponse.status).toHaveBeenCalledWith(401);
    expect(mockResponse.json).toHaveBeenCalledWith({
      error: 'You must be logged in!',
    });
  });

  it('should return an error if the token is not valid', () => {
    mockRequest.headers.authorization = 'Bearer invalidtoken';
    jwt.verify.mockImplementation((token, secret, callback) => {
      callback(new Error('Token is not valid'), null);
    });

    middleware(mockRequest, mockResponse, nextFunction);

    expect(jwt.verify).toBeCalledWith('invalidtoken', JWT_SECRET, expect.any(Function));
    expect(mockResponse.status).toHaveBeenCalledWith(401);
    expect(mockResponse.json).toHaveBeenCalledWith({
      error: 'You must be logged in! Token is not valid',
    });
  });

  it('should add user data to the request object if token is valid', async () => {
    const userData = { _id: '123', name: 'John Doe' };
    mockRequest.headers.authorization = 'Bearer validtoken';
    jwt.verify.mockImplementation((token, secret, callback) => {
      callback(null, { _id: userData._id }); // Simulate successful token verification
    });
    User.findById.mockResolvedValue(userData); // Simulate successful user data retrieval
  
    await middleware(mockRequest, mockResponse, nextFunction);
  
    expect(jwt.verify).toHaveBeenCalledWith('validtoken', JWT_SECRET, expect.any(Function));
    expect(User.findById).toHaveBeenCalledWith('123');
    expect(mockRequest.user).toEqual(userData); // Verify that user data is added to the request object
    expect(nextFunction).toHaveBeenCalled(); // Ensure that the next middleware function is called
  });

  it('should call next if token is valid', async () => {
    mockRequest.headers.authorization = 'Bearer validtoken';
    jwt.verify.mockImplementation((token, secret, callback) => {
      callback(null, { _id: '123' });
    });
    User.findById.mockResolvedValue({});

    await middleware(mockRequest, mockResponse, nextFunction);

    expect(nextFunction).toHaveBeenCalled();
  });
});
