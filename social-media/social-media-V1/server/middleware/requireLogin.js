const jwt = require('jsonwebtoken')
const {JWT_SECRET} = require('../config/keys')
const User = require('../models/user');

// Checking if user is logged in and the JWT is correct + plus save user in the request object
module.exports = (req, res, next) => {
    const {authorization} = req.headers
    if(!authorization){
        return res.status(401).json({error: "You must be logged in!"})
    }

    const token = authorization.replace("Bearer ", "")
    jwt.verify(token, JWT_SECRET, (err, payload) => {
        if(err){
            return res.status(401).json({error: `You must be logged in! ${err.message}`})
        }

        // If the token is valid, retrieve the user data from the database and add it to the request object. So it (the user data) is always awailable in the backend.
        const {_id} = payload   
        User.findById(_id)
        .then((userData) => {
            req.user = userData
            next()
        })
    })
}