const request = require('supertest');
const express = require('express');
const bodyParser = require('body-parser');
const Post = require('../models/post');
const requireLogin = jest.fn((req, res, next) => next()); // Mocking the requireLogin middleware

// Mock Post model methods
jest.mock('../models/post', () => ({
  find: jest.fn(),
  populate: jest.fn().mockReturnThis(),
  then: jest.fn().mockReturnThis(),
  catch: jest.fn(),
  save: jest.fn()
}));

// Setup Express app for testing
const app = express();
app.use(bodyParser.json());
const router = require('../routes/post'); // Adjust the path as necessary
// app.use(router);
app.use("/post", router);

describe('Post Routes', () => {
  beforeEach(() => {
    jest.clearAllMocks(); // Clear mocks between tests
  });

  describe('GET /allPosts', () => {
    it('should return all posts', async () => {
      Post.find.mockImplementationOnce(() => ({
        populate: jest.fn().mockResolvedValue([{ _id: '1', title: 'Test Post', body: 'This is a test', postedBy: { _id: 'user1', name: 'John Doe' }}])
      }));

      const response = await request(app).get('/post/allPosts');
      expect(response.statusCode).toBe(200);
      expect(response.body).toHaveProperty('posts');
      expect(response.body.posts).toHaveLength(1);
      expect(requireLogin).toBeCalled();
    });

    it('should handle errors when fetching all posts', async () => {
      Post.find.mockImplementationOnce(() => ({
        populate: jest.fn().mockRejectedValue(new Error('Database error'))
      }));

      const response = await request(app).get('/allPosts');
      expect(response.statusCode).toBeGreaterThanOrEqual(400);
      expect(response.body).toHaveProperty('error');
      expect(requireLogin).toBeCalled();
    });

    it('should return an empty list if no posts are found', async () => {
      Post.find.mockImplementationOnce(() => ({
        populate: jest.fn().mockResolvedValue([])
      }));

      const response = await request(app).get('/allPosts');
      expect(response.statusCode).toBe(200);
      expect(response.body.posts).toHaveLength(0);
      expect(requireLogin).toBeCalled();
    });
  });

  describe('POST /createPost', () => {
    it('should create a new post', async () => {
      const mockPost = { title: 'New Post', body: 'Post body', postedBy: 'user1' };
      Post.prototype.save = jest.fn().mockResolvedValue(mockPost);

      const response = await request(app)
        .post('/createPost')
        .send({ title: 'New Post', body: 'Post body' });

      expect(response.statusCode).toBe(200);
      expect(response.body).toHaveProperty('post');
      expect(requireLogin).toBeCalled();
    });

    it('should return an error if required fields are missing', async () => {
      const response = await request(app)
        .post('/createPost')
        .send({ title: 'Missing Body' }); // Body field is missing

      expect(response.statusCode).toBe(422);
      expect(response.body).toHaveProperty('error');
      expect(requireLogin).toBeCalled();
    });
  });

  describe('GET /myPosts', () => {
    it('should return posts for the logged-in user', async () => {
      Post.find.mockImplementationOnce(() => ({
        populate: jest.fn().mockResolvedValue([{ _id: '2', title: 'My Post', body: 'This is my post', postedBy: { _id: 'user1', name: 'John Doe' }}])
      }));

      const response = await request(app).get('/myPosts');
      expect(response.statusCode).toBe(200);
      expect(response.body).toHaveProperty('myPosts');
      expect(response.body.myPosts).toHaveLength(1);
      expect(requireLogin).toBeCalled();
    });

    it('should handle errors when fetching my posts', async () => {
      Post.find.mockImplementationOnce(() => ({
        populate: jest.fn().mockRejectedValue(new Error('Database error'))
      }));

      const response = await request(app).get('/myPosts');
      expect(response.statusCode).toBeGreaterThanOrEqual(400);
      expect(response.body).toHaveProperty('error');
      expect(requireLogin).toBeCalled();
    });

    it('should return an empty list if the user has no posts', async () => {
      Post.find.mockImplementationOnce(() => ({
        populate: jest.fn().mockResolvedValue([])
      }));

      const response = await request(app).get('/myPosts');
      expect(response.statusCode).toBe(200);
      expect(response.body.myPosts).toHaveLength(0);
      expect(requireLogin).toBeCalled();
    });
  });
});
