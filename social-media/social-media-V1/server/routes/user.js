const express = require('express')
const router = express.Router()
const Post = require('../models/post');
const requireLogin = require('../middleware/requireLogin')      //middleware - require user to be logged in
const User = require('../models/user');
const req = require('express/lib/request');


router.get('/user/:id', requireLogin, (req, res) => {
    User.findOne({ _id: req.params.id })
        .select("-password")
        .then(user => {
            Post.find({ postedBy: req.params.id })
                .populate("postedBy", "_id name")
                .exec()
                .then(posts => {
                    res.json({ user, posts })
                })
                .catch(err => {
                    return res.status(422).json({ error: err })
                })
        }).catch(err => {
            return res.status(404).json({ error: "User not found" })
        })
})

router.put('/follow', requireLogin, (req, res) => {
    User.findByIdAndUpdate(req.body.followId, {
        $push: { followers: req.user._id }
    }, {
        new: true
    })
        .then(result => {
            return User.findByIdAndUpdate(req.user._id, {
                $push: { following: req.body.followId }
            }, {
                new: true
            }).select("-password")
        })
        .then(result => {
            res.json(result)
        })
        .catch(err => {
            return res.status(422).json({ error: err })
        })
})

router.put('/unfollow', requireLogin, (req, res) => {
    User.findByIdAndUpdate(req.body.unfollowId, {
        $pull: { followers: req.user._id }
    }, {
        new: true
    })
        .then(result => {
            return User.findByIdAndUpdate(req.user._id, {
                $pull: { following: req.body.unfollowId }
            }, {
                new: true
            }).select("-password")
        })
        .then(result => {
            res.json(result)
        })
        .catch(err => {
            return res.status(422).json({ error: err })
        })
})

router.put('/updateProfilePicture', requireLogin, (req, res) => {
    User.findByIdAndUpdate(req.user._id, { $set: { profilePicture: req.body.profilePicture } }, { new: true })
        .then(result => {
            res.json(result)
        })
        .catch(err => {
            return res.status(422).json({ error: "Picture can not be saved," })
        })
})


router.put('/editProfile', requireLogin, (req, res) => {
    const { name, email, profilePicture } = req.body;

    if (!name || !email) {
        return res.status(400).json({ error: "Name and email are required fields" });
    }

    User.findByIdAndUpdate(req.user._id, {
        $set: {
            name: name,
            email: email,
            profilePicture: profilePicture || req.user.profilePicture
        }
    }, { new: true })
        .select("-password")
        .then(updatedUser => {
            if (!updatedUser) {
                return res.status(404).json({ error: "User not found" });
            }
            res.json(updatedUser);
        })
        .catch(err => {
            return res.status(500).json({ error: "Internal server error" });
        });
});

router.get('/user/:id/followers', requireLogin, (req, res) => {
    User.findById(req.params.id)
        .populate('followers', '_id name profilePicture')
        .select('followers')
        .then(user => {
            if (!user) {
                return res.status(404).json({ error: 'User not found' });
            }
            res.json(user.followers);
        })
        .catch(err => {
            return res.status(500).json({ error: 'Internal server error' });
        });
});

router.get('/user/:id/following', requireLogin, (req, res) => {
    User.findById(req.params.id)
        .populate('following', '_id name profilePicture')
        .select('following')
        .then(user => {
            if (!user) {
                return res.status(404).json({ error: 'User not found' });
            }
            res.json(user.following);
        })
        .catch(err => {
            return res.status(500).json({ error: 'Internal server error' });
        });
});

router.post('/search-users', (req, res) => {
    let userPattern = new RegExp("^" + req.body.query)
    User.find({ name: { $regex: userPattern } })
        .select("_id name profilePicture")
        .then(user => {
            res.json({ user })
        })
        .catch(err => {
            console.log(err)
        })
})


router.delete('/user/delete', requireLogin, async (req, res) => {
    // Ensure only the authenticated user can delete their own account
    if (req.user._id.toString() !== req.body.userId) {
        return res.status(401).json({ error: 'You are not authorized to perform this action' });
    }

    try {
        // Find and delete user by ID
        const deletedUser = await User.findByIdAndDelete(req.body.userId);

        if (!deletedUser) {
            return res.status(404).json({ error: 'User not found' });
        }

        // Delete all posts and comments by the user
        await Post.deleteMany({ postedBy: req.body.userId });
        await Post.updateMany({}, { $pull: { comments: { postedBy: req.body.userId } } });

        res.json({ message: 'User and associated posts/comments deleted successfully' });
    } catch (error) {
        console.error('Error deleting user:', error);
        res.status(500).json({ error: 'Internal server error' });
    }
});


module.exports = router