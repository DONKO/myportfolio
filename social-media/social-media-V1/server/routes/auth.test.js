const request = require("supertest");
const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const User = require("../models/user");
const authRouter = require("../routes/auth");
const { JWT_SECRET, JWT_EXPIRES_IN } = require("../config/keys");
const requireLogin = require("../middleware/requireLogin");
const app = express();

app.use(express.json());
app.use("/auth", authRouter);

mongoose.connect = jest.fn();
User.findOne = jest.fn();
User.prototype.save = jest.fn();
bcrypt.hash = jest.fn();
bcrypt.compare = jest.fn();
jwt.sign = jest.fn();

// Mock data
const mockUser = {
  _id: "mockId",
  name: "Test User",
  email: "test@example.com",
  password: "hashedpass",
};


describe("Auth Routes", () => {
  describe("POST /signup", () => {
    beforeEach(() => {
      User.findOne.mockClear();
      bcrypt.hash.mockClear();
      User.prototype.save.mockClear();
    });

    it("should return 422 when required fields are missing", async () => {
      const response = await request(app)
        .post("/auth/signup")
        .send({ name: "Test", email: "" });

      expect(response.status).toBe(422);
      expect(response.body).toEqual({
        error: "Please add all the required fields!",
      });
    });

    it("should return 422 when user with email already exists", async () => {
      User.findOne.mockResolvedValue(mockUser);

      const response = await request(app)
        .post("/auth/signup")
        .send({ name: "Test", email: "test@example.com", password: "pass123" });

      expect(response.status).toBe(422);
      expect(response.body).toEqual({
        error: "User with this email already exists.",
      });
    });

    it("should save user when valid data is provided", async () => {
      User.findOne.mockResolvedValue(null);
      bcrypt.hash.mockResolvedValue("hashedpass");
      User.prototype.save.mockResolvedValue(mockUser);

      const response = await request(app)
        .post("/auth/signup")
        .send({ name: "Test", email: "test@example.com", password: "pass123" });

      expect(User.prototype.save).toHaveBeenCalled();
      expect(response.status).toBe(200);
      expect(response.body).toEqual({ message: "Saved successfuly!" });
    });

  });

  describe("POST /signin", () => {
    beforeEach(() => {
      User.findOne.mockClear();
      bcrypt.compare.mockClear();
      jwt.sign.mockClear();
    });

    it("should return 422 when required fields are missing", async () => {
      const response = await request(app)
        .post("/auth/signin")
        .send({ email: "", password: "" });

      expect(response.status).toBe(422);
      expect(response.body).toEqual({
        error: "Please provide required fields!",
      });
    });

    it("should return 422 when user does not exist", async () => {
      User.findOne.mockResolvedValue(null);

      const response = await request(app)
        .post("/auth/signin")
        .send({ email: "nonexistent@example.com", password: "pass123" });

      expect(response.status).toBe(422);
      expect(response.body).toEqual({
        error: "Invalid login email or password.",
      });
    });

    it("should return 422 when password does not match", async () => {
      User.findOne.mockResolvedValue(mockUser);
      bcrypt.compare.mockResolvedValue(false);

      const response = await request(app)
        .post("/auth/signin")
        .send({ email: "test@example.com", password: "wrongpass" });

      expect(response.status).toBe(422);
      expect(response.body).toEqual({
        error: "Invalid login email or password.",
      });
    });

    it("should return a token when logged in successfully", async () => {
      User.findOne.mockResolvedValue(mockUser);
      bcrypt.compare.mockResolvedValue(true);
      jwt.sign.mockReturnValue("fakeToken");

      const response = await request(app)
        .post("/auth/signin")
        .send({ email: "test@example.com", password: "pass123" });

      expect(response.status).toBe(200);
      expect(response.body).toEqual({
        message: "Successfuly Logged In",
        token: "fakeToken",
      });
      expect(jwt.sign).toHaveBeenCalledWith({ _id: mockUser._id }, JWT_SECRET, {
        expiresIn: JWT_EXPIRES_IN,
      });
    });

  });

});
