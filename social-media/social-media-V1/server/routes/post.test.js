const request = require('supertest');
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const Post = require('../models/post');
jest.mock('../models/post');
const requireLogin = require('../middleware/requireLogin');
jest.mock('../middleware/requireLogin');
const postRouter = require('../routes/post');

const app = express();
app.use(bodyParser.json());
app.use(postRouter);


mongoose.connect = jest.fn();

// Ensure requireLogin is being called
beforeEach(() => {
  requireLogin.mockClear();
  Post.find.mockClear();
});

// Mock user
const user = {
  _id: new mongoose.Types.ObjectId(),
  name: 'John Doe',
  password: 'password',
};

// Mock data for tests
const posts = [
  { _id: '1', title: 'Title 1', body: 'Body 1', postedBy: user._id },
  { _id: '2', title: 'Title 2', body: 'Body 2', postedBy: user._id }
];

// Unit tests covering all scenarios
describe('Post Routes', () => {
  test('GET /allPosts should return all posts', async () => {
    Post.find.mockResolvedValue(posts);
    await request(app)
      .get('/allPosts')
      .expect(200)
      .then((res) => {
        expect(res.body.posts).toEqual(posts);
      });
  });

  test('GET /allPosts should handle errors', async () => {
    const errorMessage = 'Error getting all posts';
    Post.find.mockRejectedValue(new Error(errorMessage));
    await request(app)
      .get('/allPosts')
      .expect(500); // Or whichever status code fits your error handling
  });

  test('POST /createPost without title or body should return status 422', async () => {
    await request(app)
      .post('/createPost')
      .send({ title: '', body: '' })
      .expect(422);
  });

  test('POST /createPost with title and body should create a post', async () => {
    const newPost = { title: 'New Post', body: 'New body', postedBy: user._id };
    Post.mockImplementation(() => ({
      save: jest.fn().mockResolvedValue(newPost)
    }));

    await request(app)
      .post('/createPost')
      .send(newPost)
      .expect(200)
      .then((res) => {
        expect(res.body.post).toEqual(newPost);
      });
  });

  test('POST /createPost should handle save errors', async () => {
    const errorMessage = 'Error saving post';
    Post.mockImplementation(() => ({
      save: jest.fn().mockRejectedValue(new Error(errorMessage))
    }));

    await request(app)
      .post('/createPost')
      .send({ title: 'A title', body: 'A body' })
      .expect(500); // Or whichever status code fits your error handling
  });

  test('GET /myPosts should return posts of the current user', async () => {
    Post.find.mockResolvedValue(posts.filter(post => post.postedBy.toString() === user._id.toString()));
    await request(app)
      .get('/myPosts')
      .expect(200)
      .then((res) => {
        expect(res.body.myPosts).toEqual(posts.filter(post => post.postedBy.toString() === user._id.toString()));
      });
  });

  test('GET /myPosts should handle errors', async () => {
    const errorMessage = 'Error getting my posts';
    Post.find.mockRejectedValue(new Error(errorMessage));
    await request(app)
      .get('/myPosts')
      .expect(500); // Or whichever status code fits your error handling
  });
});
