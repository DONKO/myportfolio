const express = require('express')
const router = express.Router()
const User = require('../models/user');
const bcrypt = require('bcryptjs')      //For hashing password
const jwt = require('jsonwebtoken')     //For creating json web token
const {JWT_SECRET, JWT_EXPIRES_IN} = require('../config/keys')
const requireLogin = require('../middleware/requireLogin')

router.get('/protected', requireLogin, (req, res) => {
    res.send("hello user")
})

//sign Up route
router.post('/signup', (req, res) => {
    const {name, email, password, profilePicture} = req.body
    if(!name || !email || !password){
        return res.status(422).json({error: "Please add all the required fields!"})
    }

    User.findOne({email:email})
    .then((savedUser) => {
        if(savedUser){
            return res.status(422).json({error: "User with this email already exists."})
        }

        bcrypt.hash(password, 12)   //hashing password
        .then((hasshedPassword) =>{
            const user = new User({
                name: name,
                email: email,
                password: hasshedPassword,
                profilePicture: profilePicture
            })
            user.save()
            .then((user) => {
                res.json({message: "Saved successfuly!"})
            })
            .catch((error) => {
                console.log("Error saving user: ", error)
            })
        })

    })
    .catch((error) => {
        console.log("Error: Sign Up: ", error)
    })
})

//sign In route
router.post('/signin', (req, res) => {
    const {email, password} = req.body

    if(!email || !password){
        return res.status(422).json({error: "Please provide required fields!"})
    }

    User.findOne({email:email})
    .then((savedUser) => {
        if(!savedUser){
            return res.status(422).json({error: "Invalid login email or password."})
        }

        bcrypt.compare(password, savedUser.password)
        .then((doMatch) =>{
            if(doMatch){
                const token = jwt.sign({_id:savedUser._id}, JWT_SECRET, {       // generating and sending jwt token
                    expiresIn: JWT_EXPIRES_IN,
                })
                const {_id, name, email, followers, following, profilePicture} = savedUser
                res.json({message: "Successfuly Logged In", token: token, user: {_id, name, email, followers, following, profilePicture}})

            }
            else{
                res.status(422).json({error: "Invalid login email or password."})
            }
        })
        .catch((error) => {
            console.log("Error: Sign In: ", error)
        })

    })
})

module.exports = router