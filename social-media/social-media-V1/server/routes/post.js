const express = require('express')
const router = express.Router()
const Post = require('../models/post');
const requireLogin = require('../middleware/requireLogin')

router.get('/allPosts', requireLogin, (req, res) => {
    let page = parseInt(req.query.page) || 0;
    let limit = parseInt(req.query.limit) || 10;
    let skip = page * limit;

    Post.find()
        .populate('postedBy', '_id name profilePicture')
        .populate('comments.postedBy', '_id name profilePicture')
        .sort('-createdAt')
        .then(posts => {
            Post.countDocuments().then(count => {
                res.json({ posts: posts.slice(skip, skip + limit), hasMore: skip + limit < count });
            });
        })
        .catch(error => {
            console.log("Error getting all posts: ", error);
            res.status(500).json({error: "Internal server error"});
        })
});



router.get('/post/:postId', requireLogin, (req, res) => {
    Post.findOne({ _id: req.params.postId })
        .populate('postedBy', '_id name profilePicture')
        .populate('comments.postedBy', '_id name profilePicture')
        .then((post) => {
            res.json({ post: post })
        })
        .catch((error) => {
            console.log("Error getting post: ", error)
        })
})

router.get('/getFollowingUsersPosts', requireLogin, (req, res) => {
    let page = parseInt(req.query.page) || 0;
    let limit = parseInt(req.query.limit) || 10;
    let skip = page * limit;

    Post.find({ postedBy: { $in: req.user.following } })
        .populate('postedBy', '_id name profilePicture')
        .populate('comments.postedBy', '_id name')
        .sort('-createdAt')
        .skip(skip)
        .limit(limit)
        .then(posts => {
            Post.countDocuments({ postedBy: { $in: req.user.following } }).then(count => {
                res.json({ posts: posts, hasMore: skip + limit < count });
            });
        })
        .catch(error => {
            console.log("Error getting following users' posts: ", error);
            res.status(500).json({error: "Internal server error"});
        });
});



router.post('/createPost', requireLogin, (req, res) => {
    const { body, url } = req.body
    if (!body) {
        return res.status(422).json({ error: "Please add all required fileds!" })
    }

    req.user.password = undefined

    let post;

    if (url) {
        post = new Post({
            body: body,
            photo: url,
            postedBy: req.user
        })
    }
    else {
        post = new Post({
            body: body,
            postedBy: req.user
        })
    }

    post.save()
        .then((result) => {
            res.json({ post: result })
        })
        .catch((error) => {
            console.log("Error saving post: ", error)
        })
})

router.get('/myPosts', requireLogin, (req, res) => {
    Post.find({ postedBy: req.user._id })
        .populate('postedBy', '_id name')
        .then((myPosts) => {
            res.json({ myPosts: myPosts })
        })
        .catch((error) => {
            console.log("Error getting my posts: ", error)
        })
})

router.put('/like', requireLogin, (req, res) => {
    const postId = req.body.postId;
    const userId = req.user._id;

    // First, check if the user has already liked the post
    Post.findById(postId)
        .then(post => {
            if (post.likes.includes(userId)) {
                return res.status(403).json({ error: "You have already liked this post." });
            }

            // If the user hasn't liked the post yet, proceed to like the post
            Post.findByIdAndUpdate(postId, {
                $push: { likes: userId }
            }, { new: true })
                .populate('postedBy', 'id name profilePicture')
                .populate('comments.postedBy', '_id name')
                .exec()
                .then(result => {
                    res.json(result);
                })
                .catch(error => {
                    return res.status(422).json({ error: error });
                });
        })
        .catch(error => {
            return res.status(422).json({ error: "Post not found." });
        });
});

router.put('/unlike', requireLogin, (req, res) => {
    Post.findById(req.body.postId)
        .then(post => {
            // Check if the user's ID is in the likes array
            if (!post.likes.includes(req.user._id)) {
                return res.status(403).json({ error: "User has not liked the post." });
            }

            // If the user is in the likes array, proceed with the unlike process
            Post.findByIdAndUpdate(req.body.postId, {
                $pull: { likes: req.user._id }
            }, {
                new: true
            })
                .populate('postedBy', 'id name profilePicture')
                .populate('comments.postedBy', '_id name')
                .exec()
                .then(result => {
                    res.json(result)
                })
                .catch(error => {
                    return res.status(422).json({ error: error })
                })
        })
        .catch(error => {
            return res.status(422).json({ error: "Post not found." })
        })
});

router.put('/comment', requireLogin, (req, res) => {
    const comment = {
        text: req.body.text,
        postedBy: req.user._id
    }
    Post.findByIdAndUpdate(req.body.postId, {
        $push: { comments: comment }
    }, {
        new: true
    })
        .populate('comments.postedBy', '_id name')
        .populate('postedBy', '_id name profilePicture')
        .exec()
        .then(result => {
            res.json(result)
        })
        .catch(error => {
            return res.status(422).json({ error: error })
        })
})

router.delete('/deletePost/:postId', requireLogin, (req, res) => {
    Post.findOne({ _id: req.params.postId })
        .populate('postedBy', '_id')
        .exec()
        .then(post => {
            if (!post) {
                return res.status(422).json({ error: "Post not found" })
            }
            if (post.postedBy._id.toString() === req.user._id.toString()) {
                Post.deleteOne({ _id: req.params.postId })
                    .then(result => {
                        res.json(result)
                    })
                    .catch(error => {
                        console.log("Error deleting post: ", error)
                    })
            }
            else {
                return res.status(403).json({ error: "You do not have permission to delete this post!" })
            }
        })
        .catch(error => {
            return res.status(422).json({ error: error })
        })
})

router.delete('/deleteComment/:postId/:commentId', requireLogin, (req, res) => {
    Post.findOne({ _id: req.params.postId })
        .populate('comments.postedBy', '_id name')
        .populate('postedBy', '_id name')
        .exec()
        .then(post => {
            if (!post) {
                return res.status(422).json({ error: "Post not found" })
            }
            const comment = post.comments.find(comment => comment._id.toString() === req.params.commentId);
            if (!comment) {
                return res.status(422).json({ error: "Comment not found" })
            }
            if (comment.postedBy._id.toString() === req.user._id.toString() || post.postedBy._id.toString() === req.user._id.toString()) {
                const updatedComments = post.comments.filter(comment => comment._id.toString() !== req.params.commentId);
                post.comments = updatedComments;
                post.save()
                    .then(result => {
                        res.json(result)
                    })
                    .catch(error => {
                        console.log("Error deleting comment: ", error)
                    })
            }
            else {
                return res.status(403).json({ error: "You do not have permission to delete this comment!" })
            }
        })
        .catch(error => {
            return res.status(422).json({ error: error })
        })
})

router.put('/editPost/:postId', requireLogin, (req, res) => {
    const { body, photo } = req.body;
    const { postId } = req.params;

    if (!body) {
        return res.status(422).json({ error: "Please provide the content to update." });
    }

    Post.findById(postId)
        .exec()
        .then(post => {
            if (!post) {
                return res.status(404).json({ error: "Post not found." });
            }
            if (post.postedBy._id.toString() !== req.user._id.toString()) {
                return res.status(403).json({ error: "You do not have permission to edit this post." });
            }

            // Update the post if the user is the owner
            Post.findByIdAndUpdate(postId, { $set: { body: body, photo: photo } }, { new: true })
            .populate('postedBy', 'id name profilePicture')
            .populate('comments.postedBy', '_id name profilePicture')
                .then(result => {
                    res.json({ message: "Post updated successfully", post: result });
                })
                .catch(error => {
                    console.log("Error updating post: ", error);
                    res.status(422).json({ error: "Error updating post." });
                });
        })
        .catch(error => {
            console.log("Error finding post: ", error);
            res.status(422).json({ error: "Error finding post." });
        });
});


module.exports = router