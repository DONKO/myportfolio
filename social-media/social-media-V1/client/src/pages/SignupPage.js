import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import M from 'materialize-css';
import { validateEmail } from "../services/validationService";

const SignupPage = () => {
	const [name, setName] = useState("")
	const [password, setPasword] = useState("")
	const [email, setEmail] = useState("")
	const [image, setImage] = useState("")
	const [imageUrl, setImageUrl] = useState(undefined)

	const navigate = useNavigate()

	useEffect(() => {
		if (imageUrl) {
			uploadFields()
		}
	}, [imageUrl])

	const uploadPic = () => {
		const data = new FormData()
		data.append("file", image)
        data.append("upload_preset", "insta-clone")
        data.append("cloud_name", "dr1iyzezy")
		fetch("https://api.cloudinary.com/v1_1/dr1iyzezy/image/upload", {
			method: "post",
			body: data
		})
			.then(res => res.json())
			.then(data => {
				setImageUrl(data.url)
			})
			.catch(err => {
				console.log(err)
			})
	}

	const uploadFields = () => {
		if (!validateEmail(email)) {
            return M.toast({ html: "invalid email", classes: "#c62828 red darken-3" })
        }

		fetch("/signup", {
			method: "post",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				name,
				password,
				email,
				profilePicture: imageUrl
			})
		}).then(res => res.json())
			.then(data => {
				if (data.error) {
					M.toast({ html: data.error, classes: "#c62828 red darken-3" })
				}
				else {
					M.toast({ html: data.message, classes: "#43a047 green darken-1" })
					navigate('/login')
				}
			}).catch(err => {
				console.log(err)
			})
	}

	const postData = () => {
		if (image) {
			uploadPic()
		} else {
			uploadFields()
		}

	}

	return (
		<div className="SignupPage mycard">
			<div className="card auth-card input-field">
				<h2>Instagram</h2>
				<input
					type="text"
					placeholder="name"
					value={name}
					onChange={(e) => setName(e.target.value)}
				/>
				<input
					type="text"
					placeholder="email"
					value={email}
					onChange={(e) => setEmail(e.target.value)}
				/>
				<input
					type="password"
					placeholder="password"
					value={password}
					onChange={(e) => setPasword(e.target.value)}
				/>
				<div className="file-field input-field">
					<div className="btn #64b5f6 blue darken-1">
						<span>Upload profile picture</span>
						<input type="file" onChange={(e) => setImage(e.target.files[0])} />
					</div>
					<div className="file-path-wrapper">
						<input className="file-path validate" type="text" />
					</div>
				</div>
				<button
					className="btn waves-effect waves-light #64b5f6 blue darken-1"
					onClick={() => postData()}
				>
					SignUP
				</button>
				<h5>
					<Link to="/login">Already have an account ?</Link>
				</h5>
			</div>
		</div>
	);
};

export default SignupPage;
