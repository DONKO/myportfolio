import React, { useEffect, useState, useContext } from "react";
// import './ProfilePage.scss';
import { UserContext } from '../App';
import { useParams } from 'react-router-dom';
import fallbackProfileImage from '../data/images/fallbackProfileImage.png'
import { useUser } from "../contexts/UserContext";
import Gallery from "../components/Posts/Gallery";


const UserProfilePage = () => {
    const [userProfile, setUserProfile] = useState();
    const { state, dispatch } = useUser();
    const { userId } = useParams();
    const [showFollow, setShowFollow] = useState(false);

    useEffect(() => {
        if (state) {
            if (state.following.includes(userId)) {
                setShowFollow(false);
            } else {
                setShowFollow(true);
            }
        }
    }, [state, userId])

    useEffect(() => {
        fetch(`/user/${userId}`, {
            headers: {
                "Authorization": "Bearer " + localStorage.getItem("jwt")
            }
        }).then(res => res.json())
            .then(result => {
                setUserProfile(result);
            })
    }, [userId])

    const followUser = () => {
        fetch('/follow', {
            method: "put",
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + localStorage.getItem("jwt")
            },
            body: JSON.stringify({
                followId: userId
            })
        })
            .then(res => res.json())
            .then(data => {
                dispatch({ type: "UPDATE", payload: { following: data.following, followers: data.followers } })
                localStorage.setItem("user", JSON.stringify(data))
                setUserProfile((prevState) => {
                    return {
                        ...prevState,
                        user: {
                            ...prevState.user,
                            followers: [...prevState.user.followers, data._id]
                        }
                    }
                })
                setShowFollow(false)
            })
    }

    const unfollowUser = () => {
        fetch('/unfollow', {
            method: "put",
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + localStorage.getItem("jwt")
            },
            body: JSON.stringify({
                unfollowId: userId
            })
        })
            .then(res => res.json())
            .then(data => {
                dispatch({ type: "UPDATE", payload: { following: data.following, followers: data.followers } })
                localStorage.setItem("user", JSON.stringify(data))
                setUserProfile((prevState) => {
                    const newFollower = prevState.user.followers.filter(item => item !== data._id)
                    return {
                        ...prevState,
                        user: {
                            ...prevState.user,
                            followers: newFollower
                        }
                    }
                })
                setShowFollow(true)
            })
    }

    return (
        <>
            {userProfile ?
                <div className="ProfilePage" style={{ maxWidth: "550px", margin: "0px auto" }}>
                    <div style={{
                        margin: "18px 0px",
                        borderBottom: "1px solid grey"
                    }}>
                        <div style={{
                            display: "flex",
                            justifyContent: "space-around",
                        }}>
                            <div>
                                <img style={{ width: "160px", height: "160px", borderRadius: "80px" }}
                                    src={userProfile.user.profilePicture ? userProfile.user.profilePicture : fallbackProfileImage}
                                    alt="profile alt"
                                />
                            </div>
                            <div>
                                <h4>{userProfile?.user?.name}</h4>
                                <h4>{userProfile?.user?.email}</h4>
                                <div style={{ display: "flex", justifyContent: "space-between", width: "108%" }}>
                                    <h6>{userProfile.posts.length} posts</h6>
                                    <h6>{userProfile.user.followers.length} followers</h6>
                                    <h6>{userProfile.user.following.length} following</h6>
                                </div>
                                {showFollow ?
                                    <button style={{
                                        margin: "10px"
                                    }} className="my-primary-btn"
                                        onClick={() => followUser()}
                                    >
                                        Follow
                                    </button>
                                    :
                                    <button
                                        style={{
                                            margin: "10px"
                                        }}
                                        className="my-primary-btn"
                                        onClick={() => unfollowUser()}
                                    >
                                        UnFollow
                                    </button>
                                }
                            </div>
                        </div>
                    </div>
                    <Gallery myPosts={userProfile.posts} />
                </div>
                : <h2>Loading...</h2>}
        </>
    );
}


export default UserProfilePage;
