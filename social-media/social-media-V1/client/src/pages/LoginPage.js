import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import './LoginPage.scss';
import M from 'materialize-css';
import { useUser } from "../contexts/UserContext";
import { validateEmail } from "../services/validationService";


const LoginPage = () => {
	const [email, setEmail] = useState("");
	const [password, setPasword] = useState("");
	const navigate = useNavigate()
	const { state, dispatch } = useUser();

	const PostData = () => {
		if (!validateEmail(email)) {
            return M.toast({ html: "invalid email", classes: "#c62828 red darken-3" })
        }

		fetch("/signin", {
			method: "post",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				email,
				password,
			})
		})
			.then((res) => res.json())
			.then((data) => {
				console.log(data);
				if (data.error) {
					M.toast({ html: data.error, classes: "#c62828 red darken-3" });
				} else {
					localStorage.setItem("jwt", data.token);
					localStorage.setItem("user", JSON.stringify(data.user));
					dispatch({ type: "USER", payload: data.user })
					M.toast({ html: data.message, classes: "#43a047 green darken-1" });
					console.log("stateeee: ", state)
					navigate("/")
				}
			})
			.catch((err) => {
				console.log(err);
				M.toast({ html: err, classes: "#c62828 red darken-3" });
			});
	};


	return (
		<div className="LoginPage myCard">
			<div className="card auth-card">
				<h2>Instagram</h2>
				<input
					type="text"
					placeholder="email"
					value={email}
					onChange={(e) => setEmail(e.target.value)}
				/>
				<input
					type="password"
					placeholder="password"
					value={password}
					onChange={(e) => setPasword(e.target.value)}
				/>
				<button
					className="btn waves-effect waves-light #64b5f6 blue darken-1"
					onClick={() => PostData()}
				>
					Login
				</button>
				<h5>
					<Link to="/signup">Dont have an account ?</Link>
				</h5>
				{/*<h6>
					 <Link to="/reset">Forgot password ?</Link>
				</h6>*/}
			</div>
		</div>
	);
};

export default LoginPage;
