import React, { useState } from 'react'
import M from 'materialize-css'
import { useNavigate } from 'react-router-dom'


const CreatePost = () => {
    const navigate = useNavigate()
    const [body, setBody] = useState("")
    const [image, setImage] = useState("")

    const createPost = (url) => {
        if (url) {
            fetch("/createpost", {
                method: "post",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + localStorage.getItem("jwt")
                },
                body: JSON.stringify({
                    body: body,
                    url: url
                })
            }).then(res => res.json())
                .then(data => {

                    if (data.error) {
                        M.toast({ html: data.error, classes: "#c62828 red darken-3" })
                    }
                    else {
                        M.toast({ html: "Created post Successfully", classes: "#43a047 green darken-1" })
                        navigate('/')
                    }
                }).catch(err => {
                    console.log(err)
                })
        }
        else {
            fetch("/createpost", {
                method: "post",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + localStorage.getItem("jwt")
                },
                body: JSON.stringify({
                    body: body
                })
            }).then(res => res.json())
                .then(data => {

                    if (data.error) {
                        M.toast({ html: data.error, classes: "#c62828 red darken-3" })
                    }
                    else {
                        M.toast({ html: "Created post Successfully", classes: "#43a047 green darken-1" })
                        navigate('/')
                    }
                }).catch(err => {
                    console.log(err)
                })

        }
    }

    const postDetails = () => {
        if (image) {
            const data = new FormData()
            data.append("file", image)
            data.append("upload_preset", "insta-clone")
            data.append("cloud_name", "dr1iyzezy")
            fetch("https://api.cloudinary.com/v1_1/dr1iyzezy/image/upload", {
                method: "post",
                body: data
            })
                .then(res => res.json())
                .then(data => {
                    createPost(data.url)

                })
                .catch(err => {
                    console.log(err)
                })
        }
        else {
            createPost()
        }
    }


    return (
        <div className="card input-filed"
            style={{
                margin: "30px auto",
                maxWidth: "500px",
                padding: "20px",
                textAlign: "center"
            }}
        >
            <input
                type="text"
                placeholder="body"
                value={body}
                onChange={(e) => setBody(e.target.value)}
            />
            <div className="file-field input-field">
                <div className="btn #64b5f6 blue darken-1">
                    <span>Uplaod Image</span>
                    <input type="file" onChange={(e) => setImage(e.target.files[0])} />
                </div>
                <div className="file-path-wrapper">
                    <input className="file-path validate" type="text" />
                </div>
            </div>
            <button className="btn waves-effect waves-light #64b5f6 blue darken-1"
                onClick={() => postDetails()}

            >
                Submit post
            </button>
        </div>
    )
}


export default CreatePost;