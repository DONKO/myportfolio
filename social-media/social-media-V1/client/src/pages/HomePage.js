import React from 'react';
import Posts from '../components/Posts/Posts';
import './HomePage.scss';

const HomePage = () => {
    return (
        <div className="HomePage">
            <Posts />
        </div>
    );
};

export default HomePage;