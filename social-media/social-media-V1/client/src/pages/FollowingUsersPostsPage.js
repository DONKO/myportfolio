import React from 'react';
import './HomePage.scss';
import FollowingPosts from '../components/FollowingPosts/FollowingPosts';

const FollowingUsersPostsPage = () => {
    return (
        <div className="HomePage">
            <FollowingPosts />
        </div>
    );
};

export default FollowingUsersPostsPage;