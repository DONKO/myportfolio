import React, { useEffect, useState } from "react";
import './ProfilePage.scss';
import fallbackProfileImage from '../data/images/fallbackProfileImage.png'
import { useUser } from "../contexts/UserContext";
import Gallery from "../components/Posts/Gallery";
import EditProfile from "../components/Profile/EditProfile";
import ListOfFollowers from "../components/Profile/ListOfFollowers";
import ListOfFollowing from "../components/Profile/ListOfFollowing";
import ConfirmationModal from "../components/Modals/ConfirmationModal";
import { useNavigate } from "react-router-dom";
import M from 'materialize-css';

const ProfilePage = () => {
	const [myPosts, setMyPosts] = useState([]);
	const { state, dispatch } = useUser();
	const [isConfirmationModalOpen, setIsConfirmationModalOpen] = useState(false);
	const navigate = useNavigate();

	useEffect(() => {
		fetch('/myPosts', {
			headers: {
				"Authorization": "Bearer " + localStorage.getItem("jwt")
			}
		}).then(res => res.json())
			.then(result => {
				setMyPosts(result.myPosts);
			})
	}, [])

	const handleLogout = () => {
		localStorage.clear();
		dispatch({ type: "CLEAR" });
		navigate('/login');
	};


	const deleteUser = async (userId) => {
		try {
			const response = await fetch('/user/delete', {
				method: 'DELETE',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${localStorage.getItem('jwt')}`
				},
				body: JSON.stringify({ userId })
			});
			const data = await response.json();
			if (!response.ok) {
				M.toast({ html: `Faild to delete account: ${data.error}`, classes: "#c62828 red darken-3" });
			}
			console.log('User deleted successfully:', data, data.message);
			M.toast({ html: "User deleted successfully", classes: "#43a047 green darken-1" });
			handleLogout()

		} catch (error) {
			console.error('Error deleting user:', error.message);
			M.toast({ html: `Faild to delete account: ${error.message}`, classes: "#c62828 red darken-3" });
		}
	};

	const handleConfirmation = () => {
		deleteUser(state._id)
		setIsConfirmationModalOpen(false);
	};


	return (
		<div className="ProfilePage" style={{ maxWidth: "550px", margin: "0px auto" }}>
			{state ?
				<>
					<div style={{
						margin: "18px 0px",
						borderBottom: "1px solid grey"
					}}>
						<div style={{
							display: "flex",
							justifyContent: "space-around",

						}}>
							<div>
								<img style={{ width: "160px", height: "160px", borderRadius: "80px" }}
									src={(state && state.profilePicture) ? state.profilePicture : fallbackProfileImage}
									alt="Profile"
								/>
							</div>
							<div>
								<div className="profile-name-options-wrapper">
									<h4>{state ? state.name : "loading"}</h4>
									<div className="profile-options-wrapper">
										<EditProfile />
										<i className="material-icons modal-trigger" data-target={`confirmationModal-${state._id}`} style={{ cursor: "pointer" }} onClick={() => setIsConfirmationModalOpen(true)}>delete</i>

										{isConfirmationModalOpen &&
											<ConfirmationModal
												postId={state._id}
												message={`Are you sure you want to delete your account: ${state.name}?`}
												onConfirm={handleConfirmation}
												onClose={() => setIsConfirmationModalOpen(false)}
											/>
										}
									</div>
								</div>

								<h5>{state ? state.email : "loading"}</h5>
								<div style={{ display: "flex", justifyContent: "space-between", width: "108%" }}>
									<h6>{myPosts ? myPosts.length : "0"} posts</h6>
									<ListOfFollowers />
									<ListOfFollowing />
								</div>
							</div>
						</div>
					</div>

					<Gallery myPosts={myPosts} />
				</>
				: <h2>Loading...</h2>
			}
		</div >
	);
}

export default ProfilePage;
