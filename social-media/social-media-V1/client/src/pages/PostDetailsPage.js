import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import Post from '../components/Posts/Post';
import { useUser } from '../contexts/UserContext';

const PostDetailsPage = () => {
    const { postId } = useParams();
    const [post, setPost] = useState(null);
    const { state, dispatch } = useUser();

    const [data, setData] = useState([]);

    useEffect(() => {
        // Fetch the post details based on the postId from the server
        fetch(`/post/${postId}`, {
            headers: {
                Authorization: "Bearer " + localStorage.getItem("jwt")
            }
        })
        .then(res => res.json())
        .then(postData => {
            setPost(postData);
        })
        .catch(error => {
            console.error('Error fetching post details:', error);
        });
    }, [postId, data]);

    return (
        <div>
            {post ? (
                <Post post={post.post} userState={state} data={data} setData={setData} />
            ) : (
                <p>Loading...</p>
            )}
        </div>
    );
};

export default PostDetailsPage;
