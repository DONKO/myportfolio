import './App.scss';
import React, { useEffect } from "react";
import NavBar from "./components/Navbar/Navbar";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import HomePage from './pages/HomePage';
import ProfilePage from './pages/ProfilePage';
import UserProfilePage from './pages/UserProfilePage';
import SignupPage from './pages/SignupPage';
import LoginPage from './pages/LoginPage';
import CreatePost from './pages/CreatePost';
import FollowingUsersPostsPage from './pages/FollowingUsersPostsPage';
import { UserProvider } from './contexts/UserContext';
import { Suspense } from 'react';
import PostDetailsPage from './pages/PostDetailsPage';
import { useTheme } from './contexts/ThemeContext';


const Routing = () => {
	return (
		<Routes>
			<Route exact path="/" element={<HomePage />}></Route>
			<Route exact path="/profile" element={<ProfilePage />}></Route>
			<Route path="/profile/:userId" element={<UserProfilePage />}></Route>
			<Route path="/myFollowingPosts" element={<FollowingUsersPostsPage />}></Route>
			<Route path="/login" element={<LoginPage />}></Route>
			<Route path="/signup" element={<SignupPage />}></Route>
			<Route path="/create" element={<CreatePost />}></Route>
			<Route path="/postdetails/:postId" element={<PostDetailsPage />}></Route>
		</Routes>
	);
}

function App() {
	const { theme } = useTheme();

	// for setting background color of html element
	const setHtmlBackgroundColor = () => {
		if (theme === 'dark') {
			document.body.style.backgroundColor = 'var(--background-color-dark)';

		} else {
			document.body.style.backgroundColor = 'var(--background-color-light)';
		}
	};

	useEffect(() => {
		setHtmlBackgroundColor();
	}, [theme]);

	return (
		<UserProvider>
			<div className={`App ${theme}`}>
				<Router>
					<NavBar />
					<div className={`main-body-margin-top`}>
						<Routing />
					</div>
					
				</Router>
			</div>
		</UserProvider>
	);
}

// export default App;
export default function WrappedApp() {
	return (
		<Suspense fallback="...loading">
			<App />
		</Suspense>
	)
}
