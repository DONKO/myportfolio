import React, { useEffect, useRef, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import LikeDislike from './LikeDislike';
import Comments from './Comments';
import M from 'materialize-css';
import './Post.scss';
import EditPost from './EditPost';
import ConfirmationModal from '../Modals/ConfirmationModal';


const Post = ({ post, userState, data, setData }) => {
    const [selectedImage, setSelectedImage] = useState(null);
    const imageModalRef = useRef(null);
    const [isConfirmationModalOpen, setIsConfirmationModalOpen] = useState(false);
    const [isEditModalOpen, setIsEditModalOpen] = useState(false);
    const navigate = useNavigate()

    useEffect(() => {
        if (imageModalRef.current) {
            M.Modal.init(imageModalRef.current);
        }
    }, [imageModalRef]);

    const handleConfirmation = () => {
        deletePost(post._id);
        setIsConfirmationModalOpen(false);
    };

    const deletePost = (postId) => {
        fetch(`/deletepost/${postId}`, {
            method: "delete",
            headers: {
                Authorization: "Bearer " + localStorage.getItem("jwt")
            }
        }).then(res => res.json())
            .then(result => {
                const newData = data.filter(item => {
                    return item._id !== postId
                })
                setData(newData)
                M.toast({ html: "Post has been successfully deleted!", classes: "#43a047 green darken-1" });
                if (newData.length === 0) {
                    navigate('/profile')
                }
            })
            .catch(err => {
                console.log(err)
                M.toast({ html: "Could not delete the post!", classes: "#c62828 red darken-3" });
            })
    }

    const openImageModal = (imageUrl) => {
        setSelectedImage(imageUrl);
        const modalInstance = M.Modal.getInstance(imageModalRef.current);
        modalInstance.open();
    }

    const closeImageModal = () => {
        setSelectedImage(null);
        const modalInstance = M.Modal.getInstance(imageModalRef.current);
        modalInstance.close();
    }


    return (
        <div className="card home-card">
            <h5 style={{ padding: "5px" }}>
                <Link to={post.postedBy._id !== userState._id ? "/profile/" + post.postedBy._id : "/profile"}>
                    <img src={post.postedBy.profilePicture} className="post-card-profile-picture" alt={`${post.postedBy.name} - Alt Text`} />
                    {post.postedBy.name}
                </Link>
                {post.postedBy._id == userState._id
                    &&
                    <div className='edit-delete-post-wrapper'>
                        <Link to={`/postdetails/${post._id}`} >
                        <i className="material-icons">visibility</i>

                        </Link>
                        <i className="material-icons modal-trigger" data-target={`edit-post-modal-${post._id}`} style={{ cursor: "pointer" }} onClick={() => setIsEditModalOpen(true)}>edit</i>
                        {isEditModalOpen && (
                            <EditPost post={post} data={data} setData={setData} />
                        )}
                        <i className="material-icons modal-trigger" data-target={`confirmationModal-${post._id}`} style={{ cursor: "pointer" }} onClick={() => setIsConfirmationModalOpen(true)}>delete</i>
                    </div>

                }
            </h5>
            {post.photo &&
                <div className="card-image">
                    <i className="large material-icons modal-trigger" data-target={`image-modal-${post._id}`} onClick={() => openImageModal(post.photo)}>
                        <img src={post.photo} alt={`${post.id} - Alt Text`} />
                    </i>
                </div>
            }
            <div className="card-content">
                <div className='card-body'>{post.body}</div>
                <LikeDislike post={post} userState={userState} data={data} setData={setData} />
                <Comments comments={post.comments} postId={post._id} userState={userState} data={data} setData={setData} />
            </div>

            <div id={`image-modal-${post._id}`} className="modal image-modal" ref={imageModalRef}>
                <div className="image-close">
                    <button className="modal-close btn waves-effect waves-green" onClick={closeImageModal}>x</button>
                </div>
                <div className="fullscreen-image-wrapper">
                    <img src={selectedImage} alt="fullscreen" className="fullscreen-image" />
                </div>
            </div>

            {isConfirmationModalOpen && (
                <ConfirmationModal
                    postId={post._id}
                    message={`Are you sure you want to delete this post: ${post.body}?`}
                    onConfirm={handleConfirmation}
                    onClose={() => setIsConfirmationModalOpen(false)}
                />
            )}
        </div>
    );
};

export default Post;
