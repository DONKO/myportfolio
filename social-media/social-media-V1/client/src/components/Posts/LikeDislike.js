import React from 'react';

const LikeDislike = ({ post, userState, data, setData }) => {
    const likePost = (id) => {
        fetch('/like', {
            method: "put",
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + localStorage.getItem("jwt"),
            },
            body: JSON.stringify({
                postId: id,
            }),
        })
        .then(res => res.json())
        .then(result => {
            const newData = data.map(item => {
                if (item._id === result._id) {
                    return result;
                } else {
                    return item;
                }
            });
            setData(newData);
        })
        .catch(err => console.log(err));
    };

    const unlikePost = (id) => {
        fetch('/unlike', {
            method: "put",
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + localStorage.getItem("jwt"),
            },
            body: JSON.stringify({
                postId: id,
            }),
        })
        .then(res => res.json())
        .then(result => {
            const newData = data.map(item => {
                if (item._id === result._id) {
                    return result;
                } else {
                    return item;
                }
            });
            setData(newData);
        })
        .catch(err => console.log(err));
    };

    return (
        <div>
            {post.likes.includes(userState._id) ? (
                <i className="material-icons" style={{cursor: "pointer"}} onClick={() => unlikePost(post._id)}>thumb_down</i>
            ) : (
                <i className="material-icons" style={{cursor: "pointer"}} onClick={() => likePost(post._id)}>thumb_up</i>
            )}
            <h6>{post.likes.length} likes</h6>
        </div>
    );
};

export default LikeDislike;
