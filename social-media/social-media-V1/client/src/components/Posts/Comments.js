import React, { useState } from 'react';
import Comment from './Comment';
import './Comments.scss';

const Comments = ({ comments, postId, userState, data, setData }) => {
    const [commentText, setCommentText] = useState('');
    const [showAllComments, setShowAllComments] = useState(false);

    const makeComment = (text, postId) => {
        fetch('/comment', {
            method: "put",
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + localStorage.getItem("jwt"),
            },
            body: JSON.stringify({
                postId,
                text,
            }),
        })
        .then(res => res.json())
        .then(result => {
            const newData = data.map(item => {
                if (item._id === result._id) {
                    return result;
                } else {
                    return item;
                }
            });
            setData(newData);
            setCommentText('');
        })
        .catch(err => console.log(err));
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        makeComment(commentText, postId);
    };

    return (
        <div>
            {comments.slice(0, showAllComments ? comments.length : 1).map(comment => (
                <Comment key={comment._id} comment={comment} postId={postId} userState={userState} setData={setData} />
            ))}
            {comments.length > 1 && (
                <p className='show-all-comments' onClick={() => setShowAllComments(!showAllComments)}>
                    {showAllComments ? 'Hide Comments' : 'Show All Comments'}
                </p>
            )}
            <form onSubmit={handleSubmit}>
                <input
                    type="text"
                    placeholder="Add a comment"
                    value={commentText}
                    onChange={(e) => setCommentText(e.target.value)}
                />
            </form>
        </div>
    );
};

export default Comments;
