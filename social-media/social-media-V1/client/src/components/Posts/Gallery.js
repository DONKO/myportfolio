import React from 'react';
import { Link } from 'react-router-dom';

const Gallery = ({ myPosts }) => {
    return (
        <div className="gallery">
            {myPosts.map(item => (
                <Link key={item._id} to={`/postdetails/${item._id}`} className="item">
                    {item.photo ? (
                        <img src={item.photo} alt="my posts" />
                    ) : (
                        <div>{item.body}</div>
                    )}
                </Link>
            ))}
        </div>
    );
}

export default Gallery;
