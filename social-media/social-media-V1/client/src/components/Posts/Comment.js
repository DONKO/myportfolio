import React, { useState } from 'react';
import M from 'materialize-css';
import { useUser } from '../../contexts/UserContext';
import ConfirmationModal from '../Modals/ConfirmationModal';
import { Link } from 'react-router-dom';

const Comment = ({ comment, postId, setData }) => {
    const { state } = useUser();
    const [isConfirmationModalOpen, setIsConfirmationModalOpen] = useState(false);


    const deleteComment = (postId, commentId) => {
        fetch(`/deleteComment/${postId}/${commentId}`, {
            method: "delete",
            headers: {
                Authorization: "Bearer " + localStorage.getItem("jwt")
            }
        }).then(res => res.json())
            .then(result => {
                // Update the post's data to reflect the deleted comment
                setData(prevData => {
                    const newData = prevData.map(item => {
                        if (item._id === postId) {
                            // Filter out the deleted comment
                            return {
                                ...item,
                                comments: item.comments.filter(comment => comment._id !== commentId)
                            };
                        } else {
                            return item;
                        }
                    });
                    return newData;
                });

                M.toast({ html: "Comment deleted successfully", classes: "#43a047 green darken-1" });
            }).catch(err => {
                console.log(err);
                M.toast({ html: "An error occurred", classes: "#c62828 red darken-3" });
            });
    };

    const handleConfirmation = () => {
        deleteComment(postId, comment._id)
        setIsConfirmationModalOpen(false);
    };

    return (
        <h6>
            <Link to={comment.postedBy._id !== state._id ? "/profile/" + comment.postedBy._id : "/profile"}>
                <span style={{ fontWeight: "500" }}>{comment.postedBy.name}</span>
            </Link>
            {comment.text}
            {(comment.postedBy._id === state._id || postId === state._id) && (
                <>
                    <i className="material-icons modal-trigger" data-target={`confirmationModal-${comment._id}`} style={{ float: "right", cursor: "pointer" }} onClick={() => setIsConfirmationModalOpen(true)}>delete</i>

                    {isConfirmationModalOpen &&
                        <ConfirmationModal
                            postId={comment._id}
                            message={`Are you sure you want to delete this comment: ${comment.text}?`}
                            onConfirm={handleConfirmation}
                            onClose={() => setIsConfirmationModalOpen(false)}
                        />
                    }
                </>
            )}
        </h6>
    );
};

export default Comment;
