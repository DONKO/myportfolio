import React, { useEffect, useRef, useState } from "react";
import M from 'materialize-css';
import fallbackProfileImage from '../../data/images/fallbackProfileImage.png'

const EditPost = ({ post, data, setData }) => {
    const [body, setBody] = useState(post.body);
    const [image, setImage] = useState(post.photo);
    const editPostModalRef = useRef(null);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        if (editPostModalRef.current) {
            M.Modal.init(editPostModalRef.current);
        }
    }, [editPostModalRef]);

    const handleBodyChange = (e) => {
        setBody(e.target.value);
    };

    const handleImageChange = (inputImage) => {
        setIsLoading(true)
        const data = new FormData()
        data.append("file", inputImage)
        data.append("upload_preset", "insta-clone")
        data.append("cloud_name", "dr1iyzezy")
        fetch("https://api.cloudinary.com/v1_1/dr1iyzezy/image/upload", {
            method: "post",
            body: data
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                setImage(data.url)
                setIsLoading(false)
            })
            .catch(err => {
                console.log(err)
            })
    };

    const handleSubmit = () => {
        // Send PUT request to update post
        fetch(`/editPost/${post._id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('jwt')}`
            },
            body: JSON.stringify({ body, photo: image })
        })
            .then(response => response.json())
            .then(resData => {
                const newData = data.map(item => {
                    if (item._id === resData.post._id) {
                        return resData.post;
                    } else {
                        return item;
                    }
                });
                setData(newData);
                M.toast({ html: "Post updated Successfully", classes: "#43a047 green darken-1" });
            })
            .catch(error => {
                console.error('Error updating post:', error); // Handle error
                M.toast({ html: `Error updating post!`, classes: "#c62828 red darken-3" });
            });
    };

    const closeEditPostModal = () => {
        const modalInstance = M.Modal.getInstance(editPostModalRef.current);
        modalInstance.close();
    };

    return (
        <div>
            <div id={`edit-post-modal-${post._id}`} className="modal" ref={editPostModalRef}>
                <div className="modal-content">
                    <h4>Edit Post</h4>
                    <div className="input-field">
                        <input id="body" type="text" value={body} onChange={handleBodyChange} />
                        <label className="active" htmlFor="body">Body</label>
                    </div>
                    <div className="input-field">
                        <img style={{ width: "160px", height: "160px", borderRadius: "80px" }}
                            src={image ? image : fallbackProfileImage}
                            alt="Profile"
                        />
                        <input id="image" type="file" onChange={(e) => handleImageChange(e.target.files[0])} />
                        <label className="active" htmlFor="image">Image URL</label>
                    </div>
                </div>
                <div className="modal-footer">
                    {isLoading ?
                        <p>Loading...</p>
                        :
                        <>
                            <button className="modal-close my-primary-btn" onClick={handleSubmit}>Save</button>
                            <button className="modal-close my-secondary-btn" onClick={closeEditPostModal}>Cancel</button>
                        </>
                    }
                </div>
            </div>
        </div>
    );
};

export default EditPost;
