import React, { useEffect, useRef, useState } from "react";
import M from 'materialize-css';
import fallbackProfileImage from '../../data/images/fallbackProfileImage.png'
import { useUser } from "../../contexts/UserContext";


const ListOfFollowers = () => {
    const { state } = useUser();
    const [followers, setFollowers] = useState([]);
    const listOfFollowers = useRef(null);

    const handleModalClick = () => {
        if (!state) return;
        fetch(`/user/${state._id}/followers`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: "Bearer " + localStorage.getItem("jwt")
            }
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then(data => {
                console.log('User followers:', data);
                setFollowers(data);
            })
            .catch(error => {
                console.error('Error fetching user followers:', error);
            });
    };

    useEffect(() => {
        if (listOfFollowers.current) {
            M.Modal.init(listOfFollowers.current);
        }
    }, [listOfFollowers]);


    const closeListModal = () => {
        const modalInstance = M.Modal.getInstance(listOfFollowers.current);
        modalInstance.close();
    };

    return (
        <div>
            <i className="modal-trigger" data-target={`list-of-followers-modal`} style={{ cursor: "pointer" }} onClick={handleModalClick}>
                <h6>{state ? state.followers.length : "0"} followers</h6>
            </i>

            {followers &&
                <div id={`list-of-followers-modal`} className="modal" ref={listOfFollowers}>
                    <div className="modal-content">
                        <h4>Followers:</h4>
                        {followers.length > 0 ?
                            followers.map(follower => {
                                return (
                                    <div key={follower._id} className="collection follower">
                                        <a href={`/profile/${follower._id}`} className="collection-item">
                                            <img style={{ width: "50px", height: "50px", borderRadius: "80px" }}
                                                src={follower.profilePicture ? follower.profilePicture : fallbackProfileImage}
                                                alt="follower alt"
                                            />
                                            {follower.name}

                                        </a>
                                    </div>
                                )
                            })
                            :
                            <div >
                                You have no followers yet
                            </div>
                        }
                    </div>
                    <div className="modal-footer">
                        <button className="modal-close btn" onClick={closeListModal}>Cancel</button>
                    </div>
                </div>
            }
        </div>
    );
};

export default ListOfFollowers;
