import React, { useEffect, useRef, useState } from "react";
import M from 'materialize-css';
import fallbackProfileImage from '../../data/images/fallbackProfileImage.png'
import { useUser } from "../../contexts/UserContext";

const EditProfile = () => {
    const { state, dispatch } = useUser();
    const [name, setName] = useState("Loading");
    const [email, setEmail] = useState("Loading");
    const [image, setImage] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const editProfileModalRef = useRef(null);

    useEffect(() => {
        if (state) {
            setName(state.name)
            setEmail(state.email)
            setImage(state.profilePicture)
        }
    }, [state])

    useEffect(() => {
        if (editProfileModalRef.current) {
            M.Modal.init(editProfileModalRef.current);
        }
    }, [editProfileModalRef]);

    const handleImageChange = (inputImage) => {
        setIsLoading(true)
        const data = new FormData()
        data.append("file", inputImage)
        data.append("upload_preset", "insta-clone")
        data.append("cloud_name", "dr1iyzezy")
        fetch("https://api.cloudinary.com/v1_1/dr1iyzezy/image/upload", {
            method: "post",
            body: data
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                setImage(data.url)
                setIsLoading(false)

            })
            .catch(err => {
                console.log(err)
            })
    };

    const handleSubmit = () => {
        fetch(`/editProfile`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('jwt')}`
            },
            body: JSON.stringify({ name, email, profilePicture: image })
        })
            .then(response => response.json())
            .then(resData => {
                console.log(resData); // Handle success response
                dispatch({ type: 'USER', payload: resData });
                localStorage.setItem("user", JSON.stringify(resData));
                M.toast({ html: "Profile updated Successfully", classes: "#43a047 green darken-1" });
            })
            .catch(error => {
                console.error('Error updating profile:', error); // Handle error
                M.toast({ html: `Error updating profile!`, classes: "#c62828 red darken-3" });
            });
    };

    const closeEditProfileModal = () => {
        const modalInstance = M.Modal.getInstance(editProfileModalRef.current);
        modalInstance.close();
    };

    return (
        <div>
            <i className="material-icons modal-trigger" data-target={`edit-profile-modal`} style={{ cursor: "pointer" }}>edit</i>

            <div id={`edit-profile-modal`} className="modal" ref={editProfileModalRef}>
                <div className="modal-content">
                    <h4>Edit Profile</h4>
                    <div className="input-field">
                        <input id="name" type="text" value={name} onChange={(e) => setName(e.target.value)} />
                        <label className="active" htmlFor="name">Name</label>
                    </div>
                    <div className="input-field">
                        <input id="email" type="text" value={email} onChange={(e) => setEmail(e.target.value)} />
                        <label className="active" htmlFor="email">Email</label>
                    </div>
                    <div className="input-field">
                        <img style={{ width: "160px", height: "160px", borderRadius: "80px" }}
                            src={image ? image : fallbackProfileImage}
                            alt="Profile"
                        />
                        <input id="image" type="file" onChange={(e) => handleImageChange(e.target.files[0])} />
                        <label className="active" htmlFor="image">Image URL</label>
                    </div>
                </div>
                <div className="modal-footer">
                    {isLoading ?
                        <p>Loading...</p>
                        :
                        <>
                            <button className="modal-close my-primary-btn" onClick={handleSubmit}>Save</button>
                            <button className="modal-close my-secondary-btn" onClick={closeEditProfileModal}>Cancel</button>
                        </>
                    }
                </div>
            </div>
        </div>
    );
};

export default EditProfile;
