import React, { useEffect, useRef, useState } from "react";
import M from 'materialize-css';
import fallbackProfileImage from '../../data/images/fallbackProfileImage.png'
import { useUser } from "../../contexts/UserContext";
import './ListOfFollowing.scss';


const ListOfFollowing = () => {
    const { state, dispatch } = useUser();
    const [following, setFollowing] = useState([]);
    const listOfFollowers = useRef(null);

    const handleModalClick = () => {
        if (!state) return;
        fetch(`/user/${state._id}/following`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: "Bearer " + localStorage.getItem("jwt")
            }
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then(data => {
                console.log('User following:', data);
                setFollowing(data);
            })
            .catch(error => {
                console.error('Error fetching user following:', error);
            });
    };

    const unfollowUser = (userId) => {
        fetch('/unfollow', {
            method: "put",
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + localStorage.getItem("jwt")
            },
            body: JSON.stringify({
                unfollowId: userId
            })
        })
            .then(res => res.json())
            .then(data => {
                dispatch({ type: "UPDATE", payload: { following: data.following, followers: data.followers } })
                localStorage.setItem("user", JSON.stringify(data))
                handleModalClick()
                M.toast({ html: "Unfollowed successfully", classes: "#43a047 green darken-1" });
            })
            .catch(error => {
                console.error('Error unfollowing user:', error);
                M.toast({ html: `Error unfollowing user!`, classes: "#c62828 red darken-3" });
            });
    }

    useEffect(() => {
        if (listOfFollowers.current) {
            M.Modal.init(listOfFollowers.current);
        }
    }, [listOfFollowers]);


    const closeListModal = () => {
        const modalInstance = M.Modal.getInstance(listOfFollowers.current);
        modalInstance.close();
    };

    return (
        <div>
            <i className="modal-trigger" data-target={`list-of-following-modal`} style={{ cursor: "pointer" }} onClick={handleModalClick}>
                <h6>{state ? state.following.length : "0"} following</h6>
            </i>

            {following &&

                <div id={`list-of-following-modal`} className="modal" ref={listOfFollowers}>
                    <div className="modal-content">
                        <h4>following:</h4>
                        {following.length > 0 ?
                            following.map(follower => {
                                return (
                                    <div key={follower._id} className="collection follower">
                                        <a href={`/profile/${follower._id}`} className="collection-item">
                                            <img style={{ width: "50px", height: "50px", borderRadius: "80px" }}
                                                src={follower.profilePicture ? follower.profilePicture : fallbackProfileImage}
                                                alt="follower alt"
                                            />
                                            {follower.name}

                                        </a>
                                        <i className="material-icons unfollow" style={{ cursor: "pointer" }} onClick={() => unfollowUser(follower._id)}>delete</i>
                                    </div>
                                )
                            })
                            :
                            <div >
                                You are not following any users yet
                            </div>
                        }
                    </div>
                    <div className="modal-footer">
                        <button className="modal-close btn" onClick={closeListModal}>Cancel</button>
                    </div>
                </div>
            }
        </div>
    );
};

export default ListOfFollowing;
