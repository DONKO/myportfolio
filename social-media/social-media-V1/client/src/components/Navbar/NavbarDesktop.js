import RenderList from "./RenderList";


function NavbarDesktop() {
    return (
        <ul className="flex items-center gap-5 text-sm mr-5">
            <RenderList/>
        </ul>
    );
};

export default NavbarDesktop;