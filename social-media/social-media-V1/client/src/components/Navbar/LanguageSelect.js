import React from 'react';
import en from '../../data/images/en.svg'
import de from '../../data/images/de.png'
import { useTranslation } from 'react-i18next';
import './LanguageSelect.scss';

const LanguageSelect = () => {

    const { i18n } = useTranslation();

    const locales = {
        en: { title: 'English', image: en },
        de: { title: 'Deutsch', image: de }
    };

    return (
        <div className="">
            {Object.keys(locales).map((locale) => (
                <span key={locale}>
                    <button className='language-button' style={{ backgroundColor: i18n.resolvedLanguage === locale ? '#82c8f6' : 'transparent' }} type="submit" onClick={() => i18n.changeLanguage(locale)}>
                        <img style={{ width: "20px", height: "20px", borderRadius: "80px" }}
                            src={locales[locale].image}
                            alt="language alt"
                        />
                    </button>
                </span>
            ))}
        </div>
    );
};

export default LanguageSelect;
