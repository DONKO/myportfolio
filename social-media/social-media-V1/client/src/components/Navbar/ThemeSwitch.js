import React from 'react';
import './ThemeSwitch.scss';
import { useTheme } from '../../contexts/ThemeContext';

const ThemeSwitch = () => {
    const { theme, toggleTheme } = useTheme();

    return (
        <div className="switch">
            <input type="checkbox" className="checkbox" id="checkbox" onChange={toggleTheme} checked={theme === 'dark'}/>
                <label htmlFor="checkbox" className="label">
                    <i className="material-icons tiny" style={{cursor: "pointer", fontSize:"1rem"}}>brightness_2</i>
                    <i className="material-icons tiny" style={{cursor: "pointer", fontSize:"1rem", color:"#f1c40f"}}>wb_sunny</i>
                    <div className="ball"></div>
                </label>
        </div>
    );
};

export default ThemeSwitch;
