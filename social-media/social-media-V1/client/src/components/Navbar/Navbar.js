import React, { useEffect, useRef, useState } from "react";
import './Navbar.scss';
import { Link } from "react-router-dom";
import M from 'materialize-css'
import fallbackProfileImage from '../../data/images/fallbackProfileImage.png'
import NavbarMobile from "./NavbarMobile";
import NavbarDesktop from "./NavbarDesktop";
import { useUser } from "../../contexts/UserContext";
import { useTheme } from "../../contexts/ThemeContext";

const NavBar = () => {
    const { state } = useUser();
    const searchModal = useRef(null)
    const [search, setSearch] = useState('')
    const [userDetails, setUserDetails] = useState([])
    const { theme } = useTheme();

    useEffect(() => {
        if (searchModal.current) {
            M.Modal.init(searchModal.current);
        }
    }, [searchModal, state]);

    const fetchUsers = (query) => {
        setSearch(query)
        fetch('/search-users', {
            method: "post",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                query
            })
        }).then(res => res.json())
            .then(results => {
                setUserDetails(results.user)
            })
    }

    const [isScrolled, setIsScrolled] = useState(false);
    const [isMobile, setIsMobile] = useState(window.innerWidth <= 1024);

    useEffect(() => {   // to handle which navbar to show
        const handleResize = () => {
            setIsMobile(window.innerWidth <= 1024);
        };

        window.addEventListener('resize', handleResize);
        handleResize();

        return () => window.removeEventListener('resize', handleResize);
    }, []);

    useEffect(() => {   // to add shadow under the navbar, when user scrolls the page
        const handleScroll = () => {
            const isUserScrolled = window.scrollY > 50;
            setIsScrolled(isUserScrolled);
        };
        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    }, []);


    return (
        <div className={`navbar-wrapper-main navbar-fixed-animation ${isScrolled ? 'shadow' : ''}`}>
            <nav className={`nav-wrapper ${theme}`}>
                <div style={{ display: 'flex', gap: '10px' }}>

                    <Link to={state ? "/" : "/login"} className="main-logo">
                        Instagram
                    </Link>
                    {state &&
                        <div>
                            <i data-target="search-modal" className="large material-icons modal-trigger">search</i>
                        </div>
                    }
                </div>
                <div className="navbar-elements">
                    {isMobile ? <NavbarMobile /> : <NavbarDesktop />}
                </div>

                {state &&
                    <div id="search-modal" className="modal" ref={searchModal}>
                        <div className="modal-content">
                            <input
                                type="text"
                                placeholder="search users"
                                value={search}
                                onChange={(e) => fetchUsers(e.target.value)}
                            />
                            <ul className="collection">
                                {userDetails.map(item => {
                                    return <Link key={item._id} to={item._id !== state._id ? "/profile/" + item._id : '/profile'} onClick={() => {
                                        M.Modal.getInstance(searchModal.current).close()
                                        setSearch('')
                                    }}><li style={{ cursor: "pointer" }} className="collection-item">
                                            <img style={{ width: "50px", height: "50px", borderRadius: "80px" }}
                                                src={item.profilePicture ? item.profilePicture : fallbackProfileImage}
                                                alt="profile alt"
                                            />
                                            {item.name}
                                        </li>
                                    </Link>
                                })}

                            </ul>
                            <div className="modal-footer">
                                <button className="modal-close btn waves-effect waves-light #64b5f6 blue darken-1" onClick={() => setSearch('')}>close</button>
                            </div>
                        </div>
                    </div>
                }
            </nav>
        </div>
    );
};

export default NavBar;
