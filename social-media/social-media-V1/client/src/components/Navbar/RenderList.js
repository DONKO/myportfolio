import { Link, useNavigate } from "react-router-dom";
import useRouteConfig from "../../routes/useRouteConfig";
import ThemeSwitch from "./ThemeSwitch";
import LanguageSelect from "./LanguageSelect";
import { useState } from "react";
import { useUser } from "../../contexts/UserContext";
import ConfirmationModal from "../Modals/ConfirmationModal";


const RenderList = () => {
    const routes = useRouteConfig();
    const [isConfirmationModalOpen, setIsConfirmationModalOpen] = useState(false);
    const navigate = useNavigate();
    const { state, dispatch } = useUser();

    const handleLogout = () => {
        localStorage.clear();
        dispatch({ type: "CLEAR" });
        navigate('/login');
    };

    return (
        <>
            <ul>
                {routes.map(({ title, href, key, Icon, isModalTrigger, onClick }) => (
                    <li key={key}>
                        {onClick ? (
                            <Link onClick={() => { setIsConfirmationModalOpen(true)}} className="modal-trigger" data-target={`confirmationModal-${state._id}`}>
                            <Icon style={{ marginRight: "8px" }} />
                            {title}
                        </Link>
                        ) : (
                            <Link to={href} className={isModalTrigger ? "modal-trigger" : ""}>
                                <Icon style={{ marginRight: "8px" }} />
                                {title}
                            </Link>
                        )}
                    </li>
                ))}
                <li key={"01"}>
                    <LanguageSelect />
                </li>
                <li key={"02"}>
                    <ThemeSwitch />
                </li>
            </ul>
            {(isConfirmationModalOpen && state) &&
                <ConfirmationModal
                    postId={state._id}
                    message={`Are you sure you wan't to LogOut?`}
                    onConfirm={handleLogout}
                    onClose={() => setIsConfirmationModalOpen(false)}
                />
            }
        </>
    );
};

export default RenderList;
