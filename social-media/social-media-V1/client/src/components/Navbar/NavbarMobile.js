import React, { useRef, useState, useEffect } from "react";
import { AnimatePresence, motion } from "framer-motion";
import { Squash as Hamburger } from "hamburger-react";
import { useNavigate, Link } from 'react-router-dom';
import useRouteConfig from "../../routes/useRouteConfig";
import LanguageSelect from "./LanguageSelect";
import ThemeSwitch from "./ThemeSwitch";
import './NavbarMobile.scss';
import { useUser } from "../../contexts/UserContext";
import ConfirmationModal from "../Modals/ConfirmationModal";


function NavbarMobile() {
    const [isOpen, setOpen] = useState(false);
    const dropdownRef = useRef(null);
    const routes = useRouteConfig();
    const [isConfirmationModalOpen, setIsConfirmationModalOpen] = useState(false);
    const navigate = useNavigate();
    const { state, dispatch } = useUser();

    const handleLogout = () => {
        localStorage.clear();
        dispatch({ type: "CLEAR" });
        navigate('/login');
    };

    useEffect(() => {
        function handleClickOutside(event) {
            if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
                setOpen(false);
            }
        }

        document.addEventListener('mousedown', handleClickOutside);
        return () => {
            document.removeEventListener('mousedown', handleClickOutside);
        };
    }, [dropdownRef]);

    return (
        <div ref={dropdownRef}>
            <Hamburger toggled={isOpen} size={20} toggle={setOpen} />
            <AnimatePresence>
                {isOpen && (
                    <motion.div
                        initial={{ opacity: 0 }}
                        animate={{ opacity: 1 }}
                        exit={{ opacity: 0 }}
                        transition={{ duration: 0.2 }}
                        className="mobile-dropdown-wrapper"
                        style={{
                            position: 'fixed',
                            left: 0,
                            right: 0,
                            padding: '20px',
                            backgroundColor: '#2D3748',
                            borderBottom: '1px solid rgba(255, 255, 255, 0.2)',
                            boxShadow: '0 25px 50px -12px rgba(0, 0, 0, 1.25)',
                        }
                        }
                    >
                        <ul
                            style={{
                                display: 'grid',
                                gap: '8px',
                            }}
                        >
                            {routes.map((route, idx) => {
                                const { Icon, onClick } = route;

                                return (
                                    <motion.li
                                        initial={{ scale: 0, opacity: 0 }}
                                        animate={{ scale: 1, opacity: 1 }}
                                        transition={{
                                            type: "spring",
                                            stiffness: 260,
                                            damping: 20,
                                            delay: 0.1 + idx / 10,
                                        }}
                                        key={route.key}
                                        style={{
                                            width: '100%',
                                            padding: '1.28px',
                                            borderRadius: '12px',
                                            backgroundImage: 'linear-gradient(to top right, #1F2937, #3F4C5A, #4B5563)',
                                        }}
                                    >
                                        {onClick ? (
                                            <button
                                                onClick={() => {
                                                    setIsConfirmationModalOpen(true)
                                                }}
                                                style={{
                                                    display: 'flex',
                                                    alignItems: 'center',
                                                    justifyContent: 'space-between',
                                                    width: '100%',
                                                    padding: '20px',
                                                    borderRadius: '12px',
                                                    backgroundColor: '#2D3748',
                                                }}
                                                className="mobile-dropdown-button modal-trigger"
                                                data-target={`confirmationModal-${state._id}`}
                                            >
                                                <span
                                                    style={{
                                                        display: 'flex',
                                                        gap: '4px',
                                                        fontSize: '18px',
                                                    }}>{route.title}</span>
                                                <Icon
                                                    style={{
                                                        fontSize: '20px',
                                                    }} />
                                            </button>
                                        ) : (
                                            <Link
                                                to={route.href}
                                                onClick={() => setOpen(false)}
                                                style={{
                                                    display: 'flex',
                                                    alignItems: 'center',
                                                    justifyContent: 'space-between',
                                                    width: '100%',
                                                    padding: '20px',
                                                    borderRadius: '12px',
                                                    backgroundColor: '#2D3748',
                                                }}
                                            >
                                                <span
                                                    style={{
                                                        display: 'flex',
                                                        gap: '4px',
                                                        fontSize: '18px',
                                                    }}
                                                >{route.title}</span>
                                                <Icon
                                                    style={{
                                                        fontSize: '20px',
                                                    }}
                                                />
                                            </Link>
                                        )}
                                    </motion.li>
                                );
                            })}

                            <motion.li
                                initial={{ scale: 0, opacity: 0 }}
                                animate={{ scale: 1, opacity: 1 }}
                                transition={{
                                    type: "spring",
                                    stiffness: 260,
                                    damping: 20,
                                    delay: 0.1 + (routes.length + 1) / 10,
                                }}
                                key={"01"}
                                style={{
                                    width: '100%',
                                    padding: '1.28px',
                                    borderRadius: '12px',
                                    backgroundImage: 'linear-gradient(to top right, #1F2937, #3F4C5A, #4B5563)',
                                }}
                            >
                                <div
                                    style={{
                                        display: 'flex',
                                        alignItems: 'center',
                                        justifyContent: 'space-between',
                                        width: '100%',
                                        padding: '20px',
                                        borderRadius: '12px',
                                        backgroundColor: '#2D3748',
                                        flexDirection: 'column',
                                    }}
                                    className="language-select-wrapper"
                                >
                                    <LanguageSelect />
                                </div>
                            </motion.li>
                            <motion.li
                                initial={{ scale: 0, opacity: 0 }}
                                animate={{ scale: 1, opacity: 1 }}
                                transition={{
                                    type: "spring",
                                    stiffness: 260,
                                    damping: 20,
                                    delay: 0.1 + (routes.length + 2) / 10,
                                }}
                                key={"02"}
                                style={{
                                    width: '100%',
                                    padding: '1.28px',
                                    borderRadius: '12px',
                                    backgroundImage: 'linear-gradient(to top right, #1F2937, #3F4C5A, #4B5563)',
                                }}
                            >
                                <div
                                    style={{
                                        display: 'flex',
                                        alignItems: 'center',
                                        justifyContent: 'space-between',
                                        width: '100%',
                                        padding: '20px',
                                        borderRadius: '12px',
                                        backgroundColor: '#2D3748',
                                        flexDirection: 'column',
                                    }}
                                    className="theme-select-wrapper"
                                >
                                    <ThemeSwitch />
                                </div>
                            </motion.li>
                        </ul>
                    </motion.div>
                )}
            </AnimatePresence>

            {(isConfirmationModalOpen && state) &&
                <ConfirmationModal
                    postId={state._id}
                    message={`Are you sure you wan't to LogOut?`}
                    onConfirm={handleLogout}
                    onClose={() => setIsConfirmationModalOpen(false)}
                />
            }
        </div>
    );
};

export default NavbarMobile;











// import { useRef } from "react";
// import { useState } from "react";
// import { AnimatePresence, motion } from "framer-motion";
// import { Squash as Hamburger } from "hamburger-react";
// // import { routes } from "../routes";
// // import { ClickAwayListener } from '@mui/base/ClickAwayListener';


// function NavbarMobile() {
//     const [isOpen, setOpen] = useState(false);
//     const ref = useRef(null);

//     const handleClickAway = () => {
//         setOpen(false);
//     };

//     return (
//         // <ClickAwayListener onClickAway={handleClickAway}>
//             <div ref={ref}>
//             {/* <div ref={ref} className="lg:hidden "> */}
//                 <Hamburger toggled={isOpen} size={20} toggle={setOpen} />
//                 <AnimatePresence>
//                     {isOpen && (
//                         <motion.div
//                             initial={{ opacity: 0 }}
//                             animate={{ opacity: 1 }}
//                             exit={{ opacity: 0 }}
//                             transition={{ duration: 0.2 }}
//                             className="fixed left-0 shadow-4xl right-0 p-5 pt-0 bg-neutral-950 border-b border-b-white/20"
//                         >
//                             <ul className="grid gap-2">

//                                 {routes.map((route, idx) => {
//                                     const { Icon } = route;

//                                     return (
//                                         <motion.li
//                                             initial={{ scale: 0, opacity: 0 }}
//                                             animate={{ scale: 1, opacity: 1 }}
//                                             transition={{
//                                                 type: "spring",
//                                                 stiffness: 260,
//                                                 damping: 20,
//                                                 delay: 0.1 + idx / 10,
//                                             }}
//                                             key={route.title}
//                                             className="w-full p-[0.08rem] rounded-xl bg-gradient-to-tr from-neutral-800 via-neutral-950 to-neutral-700"
//                                         >
//                                             <a
//                                                 onClick={() => setOpen((prev) => !prev)}
//                                                 className={
//                                                     "flex items-center justify-between w-full p-5 rounded-xl bg-neutral-950"
//                                                 }
//                                                 href={route.href}
//                                             >
//                                                 <span className="flex gap-1 text-lg">{route.title}</span>
//                                                 <Icon className="text-xl" />
//                                             </a>
//                                         </motion.li>
//                                     );
//                                 })}
//                             </ul>
//                         </motion.div>
//                     )}
//                 </AnimatePresence>
//             </div>
//         // </ClickAwayListener>
//     );
// };

// export default NavbarMobile;