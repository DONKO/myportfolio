import React, { useEffect, useRef } from 'react';
import M from 'materialize-css';

const ConfirmationModal = ({ postId, message, onConfirm, onClose }) => {
    const modalRef = useRef(null);

    useEffect(() => {
        if (modalRef.current) {
            M.Modal.init(modalRef.current);
        }
    }, [modalRef]);

    const handleConfirm = () => {
        onConfirm();
        closeModal();
    };

    const closeModal = () => {
        const modalInstance = M.Modal.getInstance(modalRef.current);
        modalInstance.close();
    };

    return (
        <div ref={modalRef} id={`confirmationModal-${postId}`} className="modal">
            <div className="modal-content">
                <h4>{message}</h4>
            </div>
            <div className="modal-footer">
                <button className="modal-close my-primary-btn" onClick={closeModal}>Cancel</button>
                <button className="my-secondary-btn" onClick={handleConfirm}>Confirm</button>
            </div>
        </div>
    );
};

export default ConfirmationModal;
