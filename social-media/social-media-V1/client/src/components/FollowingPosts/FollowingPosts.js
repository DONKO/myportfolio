import React, { useState, useEffect } from 'react';
import Post from '../Posts/Post';
import { useUser } from '../../contexts/UserContext';

const FollowingPosts = () => {
    const [data, setData] = useState([]);
    const [page, setPage] = useState(0);
    const [hasMore, setHasMore] = useState(true);
    const [loading, setLoading] = useState(false);
    const { state } = useUser();

    useEffect(() => {
        if (hasMore && !loading) {
            setLoading(true);
            fetch(`/getFollowingUsersPosts?page=${page}&limit=10`, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem('jwt')
                }
            })
                .then(res => res.json())
                .then(result => {
                    const newPosts = result.posts;
                    if (newPosts.length === 0) {
                        setHasMore(false);
                    } else {
                        const combinedPosts = [...data, ...newPosts];
                        const uniquePosts = Array.from(new Map(combinedPosts.map(post => [post._id, post])).values());
                        setData(uniquePosts);
                    }
                    setLoading(false);
                })
                .catch(err => {
                    console.log("Error loading posts", err);
                    setLoading(false);
                });
        }
    }, [page]);

    useEffect(() => {
        const handleScroll = () => {
            if (window.innerHeight + document.documentElement.scrollTop + 1 >= document.documentElement.scrollHeight) {
                if (hasMore && !loading) {
                    setPage(prevPage => prevPage + 1);
                }
            }
        };

        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    }, [hasMore, loading]);

    return (
        <>
            {data.length > 0 ? data.map(item => (
                <Post key={item._id} post={item} userState={state} data={data} setData={setData} />
            )) : <h2>Loading...</h2>}
            {!hasMore && <p className='no-more-posts'>No more posts to load</p>}
        </>
    );
};

export default FollowingPosts;
