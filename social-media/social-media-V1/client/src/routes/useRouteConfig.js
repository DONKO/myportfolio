import { useNavigate } from 'react-router-dom';
import { CiViewList } from "react-icons/ci";
import { FaRegUser } from "react-icons/fa";
import { FiPlusCircle } from "react-icons/fi";

import { IoMdLogOut } from "react-icons/io";
import { IoMdLogIn } from "react-icons/io";
import { MdOutlineAccessibilityNew } from "react-icons/md";
import { useUser } from '../contexts/UserContext';
import { useTranslation } from 'react-i18next';


const useRouteConfig = () => {
    const { state } = useUser();
    const { t } = useTranslation();

    const authenticatedRoutes = [
        { title: `${t('navbar.following')}`, href: "/myFollowingPosts", key: "following", Icon: CiViewList },
        { title: state?.name, href: "/profile", key: "profile", Icon: FaRegUser },
        { title: "Create Post", href: "/create", key: "create", Icon: FiPlusCircle },
        { title: "Logout", onClick: "handleLogout", key: "logout", Icon: IoMdLogOut }
    ];

    const unauthenticatedRoutes = [
        { title: "Login", href: "/login", key: "login", Icon: IoMdLogIn },
        { title: "Signup", href: "/signup", key: "signup", Icon: MdOutlineAccessibilityNew }
    ];

    return state ? authenticatedRoutes : unauthenticatedRoutes;
};

export default useRouteConfig;