import { createContext, useContext, useEffect, useReducer } from 'react';
import { userReducer, initialState } from '../recucers/userReducer'

export const UserContext = createContext();

function parseJwt(token) {
    if (!token) { return; }
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64));
}

export function UserProvider({ children }) {
    const [state, dispatch] = useReducer(userReducer, initialState);

    useEffect(() => {
        const user = localStorage.getItem("user") ? JSON.parse(localStorage.getItem("user")) : null;
        const jwt = localStorage.getItem('jwt')

        if (user && jwt) {
            // Decode the JWT token to check expiration
            const decodedToken = parseJwt(jwt)
            const currentTime = Date.now() / 1000; // Convert milliseconds to seconds
            console.log(currentTime)
            console.log(decodedToken.exp)
            console.log(currentTime > decodedToken.exp)

            if (decodedToken && currentTime > decodedToken.exp) {
                // Token has expired, clear user state
                dispatch({ type: 'CLEAR' });
                localStorage.removeItem('user');
                window.location.href = '/login';
            } else {
                dispatch({ type: 'USER', payload: user });
            }
        } else {
            const currentPath = window.location.href.split('/').pop();
            if (currentPath !== 'login' && currentPath !== 'signup') {
                window.location.href = '/login';
            }
        }
    }, [])

    return <UserContext.Provider value={{ state, dispatch }}>{children}</UserContext.Provider>;
}

// Custom hook to use user context
export function useUser() {
    return useContext(UserContext);
}