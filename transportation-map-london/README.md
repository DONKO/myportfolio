# my-transport-map


## Run the app
- install dependencies: npm install
- start the application: npm start

## Functionalities

- User can view the map of the world. He can zoom in and out and move anywhere.
- User can input a name of a station in London.
- Suggestions will appear when the user is typing.
- User clicks on the suggestion...
- Map focuses on that location and a marker is placed.
- Up to a number of routes of public transport will appear on the map, that are going through that station.
- When hovering the station marker or the routes some information is displayed.
- User can search for a new station and when selected the old information is removed and new information is shown as before.