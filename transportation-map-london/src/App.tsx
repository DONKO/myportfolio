import './App.css';
import { MantineProvider } from '@mantine/core';
import Map from './components/Map/Map';
import Search from './components/Search/Search';
import '@mantine/core/styles.css';
import { StationProvider } from './contexts/StationContext';

function App() {
  return (
    <div className="App">
      <MantineProvider>
        <StationProvider>
          <Search />
          <Map />
        </StationProvider>
      </MantineProvider>
    </div>
  );
}

export default App;
