import React, { useState, useEffect } from 'react';
import { DeckGL } from '@deck.gl/react';
import { useStation } from '../../contexts/StationContext';
import MainTileLayer from './MainTileLayer';
import StationMarker from './StationMarker';
import useLineLayers from '../../hooks/useLineLayers';
import { getTooltip } from '../../utils/tooltipUtils';
import { ViewState } from '../../types/MapTypes';

const Map: React.FC = () => {
    const { selectedStation } = useStation();
    const layers = useLineLayers(selectedStation);

    const initialViewState: ViewState = {
        longitude: -0.1276, // Longitude of London
        latitude: 51.5072,  // Latitude of London
        zoom: 10,
        pitch: 0,
        bearing: 0,
        transitionDuration: 1000
    };

    const [viewState, setViewState] = useState<ViewState>(initialViewState);

    useEffect(() => {
        if (selectedStation) {
            setViewState({
                ...viewState,
                longitude: selectedStation.lon,
                latitude: selectedStation.lat,
                zoom: 12,
                transitionDuration: 1000
            });
        }
    }, [selectedStation]);

    
    return (
        <DeckGL
            viewState={viewState}
            onViewStateChange={({ viewState }) => setViewState(viewState as ViewState)}
            controller={true}
            layers={[MainTileLayer(), ...layers, selectedStation && StationMarker({ station: selectedStation })].filter(Boolean)}
            style={{ width: '100%', height: '100vh' }}
            getTooltip={getTooltip}
        />
    );
};

export default Map;
