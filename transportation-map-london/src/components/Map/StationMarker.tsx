import { IconLayer } from '@deck.gl/layers';
import { Station } from '../../types/MapTypes';

const StationMarker = ({ station }: { station: Station }) => {
    return new IconLayer<Station>({
        id: 'icon-layer',
        data: [station],
        getPosition: (d: Station) => [d.lon, d.lat],
        getColor: (d: Station) => [255, 0, 0],
        getIcon: (d: Station) => 'marker',
        iconAtlas: 'https://raw.githubusercontent.com/visgl/deck.gl-data/master/website/icon-atlas.png',
        iconMapping: 'https://raw.githubusercontent.com/visgl/deck.gl-data/master/website/icon-atlas.json',
        getSize: 40,
        pickable: true
    });
};

export default StationMarker;
