import { TileLayer } from '@deck.gl/geo-layers';
import { BitmapLayer } from '@deck.gl/layers';

const MainTileLayer = () => {
    return new TileLayer({
        id: 'tile-layer',
        data: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
        maxZoom: 19,
        minZoom: 0,
        renderSubLayers: props => {
            const { boundingBox } = props.tile;
            return new BitmapLayer(props, {
                data: undefined,
                image: props.data,
                bounds: [boundingBox[0][0], boundingBox[0][1], boundingBox[1][0], boundingBox[1][1]]
            });
        },
        pickable: true
    });
};

export default MainTileLayer;
