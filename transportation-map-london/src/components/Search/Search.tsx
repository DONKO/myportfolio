import { useState, useEffect, useRef } from 'react';
import { Autocomplete } from '@mantine/core';
import { debounce } from 'lodash';
import { useStation } from '../../contexts/StationContext';
import { Station } from '../../types/MapTypes';


const Search = () => {
    const [value, setValue] = useState<string>('');
    const [data, setData] = useState<Station[]>([]);
    const { setSelectedStation } = useStation();

    const debouncedFetchData = useRef(debounce((query: string) => {
        if (query.trim() === '') {
            setData([]);
            return;
        }

        fetch(`https://api.tfl.gov.uk/StopPoint/Search/${query}`)
            .then((response) => response.json())
            .then((result) => {
                const stationCount = new Map();
                const stations: Station[] = result.matches.map((station: Station) => {
                    // Count occurrences of each station name
                    let uniqueName = station.name;
                    const count = stationCount.get(station.name) || 0;
                    stationCount.set(station.name, count + 1);

                    // If this name has appeared before, append the id to make it unique
                    if (count > 0) {
                        uniqueName = `${station.name} (${station.id})`;
                    }

                    return {
                        name: uniqueName,
                        id: station.id,
                        zone: station.zone,
                        lat: station.lat,
                        lon: station.lon
                    };
                });
                setData(stations);
            })
            .catch((error) => {
                console.error('Failed to fetch stations:', error);
                setData([]);
            });
    }, 300)).current; // Debounce period in milliseconds

    useEffect(() => {
        return () => {
            debouncedFetchData.cancel();
        };
    }, [debouncedFetchData]);

    useEffect(() => {
        debouncedFetchData(value);
    }, [value, debouncedFetchData]);

    
    return (
        <div className="flex w-full justify-center mt-2.5 z-[99]">
            <div className="z-[99] w-1/2">
                <Autocomplete
                    value={value}
                    placeholder="Search for stations"
                    data={data.map((station) => station.name)} // Pass only station names to the Autocomplete
                    onChange={(itemValue) => {
                        setValue(itemValue); // Set the input value to the selected item's display name
                        const selectedStation = data.find(station => station.name === itemValue);
                        if (selectedStation) {
                            setSelectedStation(selectedStation);
                        }
                    }}
                    limit={10} // Optional: Limit the number of suggestions displayed
                />
            </div>
        </div>
    );
}

export default Search;