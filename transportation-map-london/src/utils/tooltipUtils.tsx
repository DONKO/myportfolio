import { PickingInfo } from '@deck.gl/core';
import { Line, Station } from '../types/MapTypes';

export const getTooltip = ({ object }: PickingInfo<Line | Station>): { text: string; style: { [key: string]: string } } | null => {

    // Type guards to determine the type
    function isLine(object: any): object is Line {
        return object && object.position !== undefined;
    }

    function isStation(object: any): object is Station {
        return object && object.name !== undefined;
    }

    if (object) {
        // Check if the hovered object is part of a line layer
        if (isLine(object)) {
            return {
                text: `${object.id.toUpperCase()}`,
                style: tooltipStyle
            };
        }
        // Check if the hovered object is a station
        else if (isStation(object)) {
            return {
                text: `Station: ${object.name} (ID: ${object.id})`,
                style: tooltipStyle
            };
        }
    }
    return null;
};

const tooltipStyle = {
    backgroundColor: 'rgba(35, 35, 35, 0.9)',
    color: 'white',
    fontSize: '12px',
    padding: '4px 8px',
    borderRadius: '4px',
    pointerEvents: 'none'
};
