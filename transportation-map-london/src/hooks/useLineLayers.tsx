import { useEffect, useState } from 'react';
import { PathLayer } from '@deck.gl/layers';
import { Line, Station } from '../types/MapTypes';

const useLineLayers = (selectedStation: Station | null) => {
    const [layers, setLayers] = useState<PathLayer[]>([]);

    useEffect(() => {
        if (selectedStation) {
            setLayers([]);
            fetch(`https://api.tfl.gov.uk/StopPoint/${selectedStation.id}`)
                .then(res => res.json())
                .then(data => {
                    data.lines.slice(0, 15).forEach((line: Line) => {
                        fetchDataAndCreateLayer(line.id, setLayers);
                    });
                })
                .catch(err => console.error("Error fetching station data:", err));
        }
    }, [selectedStation]);

    return layers;
};

const fetchDataAndCreateLayer = (lineId: string, setLayers: React.Dispatch<React.SetStateAction<PathLayer[]>>) => {
    fetch(`https://api.tfl.gov.uk/Line/${lineId}/Route/Sequence/inbound`)
        .then(res => res.json())
        .then(data => {
            const parsedData = data.lineStrings.map((lineString: string) => {
                try {
                    const coords = JSON.parse(lineString);
                    return coords.map((coord: [number, number]) => ({ position: coord, id: lineId }));
                } catch (error) {
                    console.error("Error parsing line string:", error);
                    return [];
                }
            }).flat();
            const newLayer = new PathLayer({
                id: `path-layer-${lineId}`,
                data: parsedData,
                getColor: () => getRandomColor(),
                getPath: d => d.position,
                getWidth: 70,
                pickable: true
            });
            setLayers(prevLayers => [...prevLayers, newLayer]);
        })
        .catch(err => console.error(`Error fetching data for line ${lineId}:`, err));
};

const getRandomColor = (): [number, number, number] => {
    return [
        Math.floor(Math.random() * 255),
        Math.floor(Math.random() * 255),
        Math.floor(Math.random() * 255)
    ];
};

export default useLineLayers;
