export type ViewState = {
    longitude: number;
    latitude: number;
    zoom: number;
    pitch: number;
    bearing: number;
    
    transitionDuration: number;
};

export type Station = {
    name: string;
    id: string;
    zone: string;
    lat: number;
    lon: number;
};

export type Line = {
    id: string;
    name: string;
    position: [number, number];
};

export type BartLine = {
    lineStrings: string[];
};
