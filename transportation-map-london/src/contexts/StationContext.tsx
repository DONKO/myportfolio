import React, { createContext, useContext, useState, ReactNode } from 'react';
import { Station } from '../types/MapTypes';


type StationContextType = {
    selectedStation: Station | null;
    setSelectedStation: (station: Station | null) => void;
};

const StationContext = createContext<StationContextType | undefined>(undefined);

export const useStation = () => useContext(StationContext) as StationContextType;

type Props = {
    children: ReactNode;
};

export const StationProvider: React.FC<Props> = ({ children }) => {
    const [selectedStation, setSelectedStation] = useState<Station | null>(null);

    return (
        <StationContext.Provider value={{ selectedStation, setSelectedStation }}>
            {children}
        </StationContext.Provider>
    );
};
