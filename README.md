# myPortfolio

In this repository, you can find my demo portfolio projects.
Note that they are not production-ready. 
Missing some confirmation modals, components could be restructured a bit,... and of course endless possibilities of features.

## Structure
```
Each folder contains a separate portfolio project.
Each subfolder (if existing) contains a different version of the project.
Check the overview of the projects below in the Projects section.
```
## General Setup and Installation
```
- Clone the repository: git clone https://gitlab.com/DONKO/myportfolio.git
- Move into a desired project: cd <project name>
- If the project folder has multiple versions move into the desired version: cd <project name version>
- Read the selected project README file for further instructions

```

## Projects

### Shopping List React App:

- Tech Stack: 
    - Frontend only: React, Typescript, SCSS, Tailwind.
- installation:
```
when you have cloned this repository go into the project shopping-list by typing
cd shopping-list-app
there you will find a README file describing the project features, installation, and setup guide as well as differences between versions.
```

### Web Shop App:

- Tech Stack: 
    - Frontend: Next.js, JavaScript, SCSS.
    - Backend: Node.js, Express.js, MySQL, jsonwebtoken, jest (for testing).
- installation:
```
when you have cloned this repository go into the project web-shop by typing
cd web-shop
there you will find a README file describing the project features, installation, and setup guide,...
```

### Social media:

- Tech Stack: 
    - Frontend: React, JavaScript, SCSS, Materialize, i18next.
    - Backend: Node.js, Express.js, Mongoose, jsonwebtoken.
- installation:
```
when you have cloned this repository go into the project social-media by typing
cd social-media
there you will find a README file describing the project features, installation, and setup guide as well as differences between versions.
```

### Transportation Map:

- Tech Stack: 
    - Frontend: React, TypeScript, Mantine, Tailwind, deck.gl.
- installation:
```
when you have cloned this repository go into the project transportation map by typing
cd transportation-map-london
there you will find a README file describing the project features, installation, and setup guide,...
```
